﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class ZipCodeResult
    {
        public string ReturnCode { get; set; }
        public string Digit { get; set; }
    }
}

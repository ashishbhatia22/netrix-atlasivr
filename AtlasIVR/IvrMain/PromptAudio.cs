﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class PromptAudio
    {
        public string PromptText { get; set; }
        public string PromptAudioPath { get; set; }
    }
}

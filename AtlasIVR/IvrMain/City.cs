﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class CityState
    {
        public string CityName { get; set; }
        public string StateName { get; set; }
    }


    public class City
    {

       
        public async Task<List<CityState>> LoadCity()
        {
            List<CityState> cities = new List<CityState>();

            foreach (string s in StateArray.Abbreviations())
            {
                // for every state, make a call
                            using (var client = new HttpClient())
            {
                string uri = String.Format("http://api.sba.gov/geodata/city_links_for_state_of/{0}/",s);

                client.BaseAddress = new Uri(uri);
                client.DefaultRequestHeaders.Accept.Clear();
              //  client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                               
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    XDocument doc;


                    using (Stream responseStream =  await response.Content.ReadAsStreamAsync())
                    {
                        doc = XDocument.Load(responseStream);
                        List<CityState> selectedStateCities = new List<CityState>();
                        selectedStateCities = (from item in doc.Root.Elements("site")
                                     select new CityState
                                     {
                                         CityName = (string)item.Element("name"),
                                        StateName =  (string)item.Element("state_abbreviation")                                     
                                     }).ToList<CityState>();

                        cities.AddRange(selectedStateCities);


                     //   var query = from t in doc.Descendants("site")                       
                     //               select t.Value;
 
                     //var k = query.ToList<string>();



                    }
                        
                       
                }
            }
            }

            return cities;

        }



    }
}

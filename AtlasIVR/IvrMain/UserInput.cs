﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class UserInput
    {
        public bool IsDtmf { get; set; }
        public string Text { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class AlphaDigitMapping
    {
        public string Alpha { get; set; }
        public string Digit { get; set; }
    }
}

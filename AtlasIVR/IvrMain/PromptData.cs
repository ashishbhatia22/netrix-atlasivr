﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class PromptData
    {
        public string ReferenceNumberPending {get;set;}

        public string FinalConfirmation {get;set;}

         public string InitialPrompt {get;set;}
         public string ReferenceNumberNotFound {get;set;}

         public string First2Digits {get;set;}


         public string NoReferenceNumber {get;set;}

         public string AlphaCharacters {get;set;}

         public string Last5Numbers {get;set;}

         public string TryAgain {get;set;}

         public string DidNotUnderstand {get;set;}

         public string PoundConfirmation {get;set;}

         public string Confirmation { get; set; }

        public string Press1Confirmation { get; set; }

        public string Press2Confirmation { get; set; }

        public string Press3Confirmation { get; set; }

        public string Press4Confirmation { get; set; }

        public string FirstAlphaDigit { get; set; }

        public string SecondAlphaDigit { get; set; }

        public string NotAbleToUnderstand { get; set; }

        public string Disconnecting { get; set; }

        public string AskUserToWait { get; set; }

        public string AskZipCode { get; set; }

        public string AgentNotFound { get; set; }

        
 
    }
}

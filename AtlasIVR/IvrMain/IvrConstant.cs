﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunicationServiceWithConsole.IvrMain
{
    public static class IvrConstant
    {
        public const string INITIAL_PROMPT_TIMER_ELAPSED = "INITIAL_PROMPT_TIMER_ELAPSED";
        public const string FIRST_2DIGITS = "INITIAL_FIRST2DIGITS";
        public const string ALPHADIGITS = "ALPHADIGITS";
        public const string LAST5DIGITS = "LAST5DIGITS";

        public const string CULTURE_NAME_SPANISH = "es-MX";
        public const string CULTURE_NAME_ENGLIGH = "en-EN";
    }
}

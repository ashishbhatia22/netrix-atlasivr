﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class IvrReport
    {
        public string CallerId { get; set; }
        public string Language { get; set; }

        public DateTime CallStartTime { get; set; }

        public DateTime CallEndTime { get; set; }

        public bool AgentTransfer { get; set; }

        public string AgentName { get; set; }

        public string AgentPhone { get; set; }

        public string AgentAddress { get; set; }

        public string WorkFlowStatus { get; set; }


        public string ReferenceNumber { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class IvrDA
    {
        public void SaveReportData(IvrReport ivrReport)
        {
            try
            {
                string connectionString = @"Server=HDQ-LUCMADEV-01\SQLEXPRESSLOCAL;Database=IVRReport;Trusted_Connection=True;";
            //Data Source=HDQ-LUCMADEV-01\SQLEXPRESSLOCAL;Initial Catalog=IVRReport;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False
                
                // Write code to add data to SQL server
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(@"INSERT INTO IVRReport (CallerId, StartTime, IvrLanguage,ReferenceNumber,EndTime,AgentName,AgentAddress,AgentPhoneNumber,IvrWorkFlowStatus,AgentTransfer) 
                VALUES (@CallerId, @StartTime, @IvrLanguage,@ReferenceNumber,@EndTime,@AgentName,@AgentAddress,@AgentPhoneNumber,@IvrWorkFlowStatus,@AgentTransfer)");
                
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                cmd.Parameters.Add("@CallerId", SqlDbType.VarChar);
                cmd.Parameters.Add("@StartTime", SqlDbType.DateTime);
                cmd.Parameters.Add("@IvrLanguage", SqlDbType.VarChar);
                cmd.Parameters.Add("@ReferenceNumber", SqlDbType.VarChar);
                cmd.Parameters.Add("@EndTime", SqlDbType.DateTime);
                cmd.Parameters.Add("@AgentName", SqlDbType.VarChar);
                cmd.Parameters.Add("@AgentAddress", SqlDbType.VarChar);
                cmd.Parameters.Add("@AgentPhoneNumber", SqlDbType.VarChar);
                cmd.Parameters.Add("@IvrWorkFlowStatus", SqlDbType.VarChar);
                cmd.Parameters.Add("@AgentTransfer", SqlDbType.VarChar);
                // Add Caller Id
                if (ivrReport.CallerId == null)
                {
                    cmd.Parameters["@CallerId"].Value = DBNull.Value;
                }
                else 
                {
                    cmd.Parameters["@CallerId"].Value =  ivrReport.CallerId;
                }
                //Add Start Time
                if (ivrReport.CallStartTime == null)
                {
                    cmd.Parameters["@StartTime"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@StartTime"].Value = ivrReport.CallStartTime;
                }

                //Add Language
                if (ivrReport.Language == null)
                {
                    cmd.Parameters["@IvrLanguage"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@IvrLanguage"].Value = ivrReport.Language;
                    
                }

                // Add reference number
                if (ivrReport.ReferenceNumber == null)
                {
                    cmd.Parameters["@ReferenceNumber"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@ReferenceNumber"].Value = ivrReport.ReferenceNumber;                   
                }

                // Add end time
                if (ivrReport.CallEndTime == null)
                {
                    cmd.Parameters["@EndTime"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@EndTime"].Value = ivrReport.CallEndTime;                     
                }

                // Add agent name time
                if (ivrReport.AgentName == null)
                {
                    cmd.Parameters["@AgentName"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@AgentName"].Value = ivrReport.AgentName;                    
                }

                // Add agent address
                if (ivrReport.AgentAddress == null)
                {
                    cmd.Parameters["@AgentAddress"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@AgentAddress"].Value = ivrReport.AgentAddress;                   
                }

                // Add agent phone number
                if (ivrReport.AgentPhone == null)
                {
                    cmd.Parameters["@AgentPhoneNumber"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@AgentPhoneNumber"].Value = ivrReport.AgentPhone;
                }

                // Add workflow status
                if (ivrReport.WorkFlowStatus == null)
                {
                    cmd.Parameters["@IvrWorkFlowStatus"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["@IvrWorkFlowStatus"].Value =  ivrReport.WorkFlowStatus;
                }

                // Add workflow status
                if (ivrReport.AgentTransfer == null)
                {
                    cmd.Parameters["@AgentTransfer"].Value = DBNull.Value;
                }
                else
                {
                    if (ivrReport.AgentTransfer)
                    {
                        cmd.Parameters["@AgentTransfer"].Value = 1;
                    }
                    else
                    {
                        cmd.Parameters["@AgentTransfer"].Value = 0;
                    }
                    
                }


                connection.Open();
                cmd.ExecuteNonQuery();
            }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

    }
}

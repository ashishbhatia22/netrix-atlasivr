﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Rtc.Collaboration.AudioVideo;

using Microsoft.Speech.Recognition;
using Microsoft.Speech.Synthesis;
using Microsoft.Speech.AudioFormat;
using log4net;
using System.Threading;
using Timers = System.Timers;
using System.Speech;
using Microsoft.Rtc.Collaboration;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class IncomingCallMain
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IncomingCallMain));
        //  private AudioVideoFlow _audioVideoFlow;
        private SpeechSynthesisConnector _speechSynthesisConnector;
        private SpeechSynthesizer _speechSynthesizer;
        private SpeechRecognitionEngine _speechRecognitionEngine;
        private SpeechRecognitionConnector _speechRecognitionConnector;
        private DtmfRecognitionEngine _dtmfRecognitionEngine;
        private AudioVideoCall _avCall;
        private AudioVideoFlow _avCallFlow;
        private CallWorkFlowState currentState = CallWorkFlowState.LanguagePrompt;
        private Dictionary<string, PromptAudio> promptAudioData = new Dictionary<string, PromptAudio>();
        private Dictionary<CallWorkFlowState, int> timerElapsedCounter = new Dictionary<CallWorkFlowState, int>();
        private Timers.Timer _idleTimer;
        private IvrWorkFlow _workFlow;
        private IvrHelper _helper;
        private AutoResetEvent _waitForRecoCompleted = new AutoResetEvent(false);
        private AutoResetEvent waitForAudioVideoFlowStateChangedToActiveCompleted = new AutoResetEvent(false);
        private AutoResetEvent waitForAudioVideoCallEstablished = new AutoResetEvent(false); 
        private AutoResetEvent[] waitToProcessCall;
        private int _silentAttempt = 0;
        Dictionary<string, List<AlphaDigitMapping>> alphaDigitMaster;
        bool _timeElapsedLanguagePrompt = false;
        private IvrReport _ivrReport;
        public IncomingCallMain()
        {
            LoadData();
        }
        
        #region Public Methods

        public void PromptUserForInput(AudioVideoCall avCall)
        {
            //Thread.CurrentThread.Name = avCall.CallId;

            _logger.Debug("ThreadId: " + Thread.CurrentThread.ManagedThreadId);
            // Log data for ivr report
            _ivrReport = new IvrReport();
            _ivrReport.CallerId = avCall.OriginalDestinationUri.Substring(6,10);
            _ivrReport.CallStartTime = DateTime.Now;

            _avCall = avCall;
            avCall.Flow.StateChanged += new EventHandler<MediaFlowStateChangedEventArgs>(OnFlowStateChanged);
            avCall.StateChanged += avCall_StateChanged;
            _speechRecognitionConnector = new SpeechRecognitionConnector();
            _speechRecognitionConnector.AttachFlow(avCall.Flow);
            // Create a speech synthesis connector and attach the AudioVideoFlow instance to it.
            _speechSynthesisConnector = new SpeechSynthesisConnector();
            _speechSynthesisConnector.AttachFlow(avCall.Flow);
            _speechSynthesisConnector.Start();

            InitializeEngine();
            
            // Create a ToneController and attach to AVFlow
            ToneController toneController = new ToneController();
            toneController.AttachFlow(avCall.Flow);
            // Subscribe to callback to receive tones
            toneController.ToneReceived += new EventHandler<ToneControllerEventArgs>(ToneController_ToneReceived);

            _helper = new IvrHelper(_speechSynthesizer, _speechRecognitionEngine, _avCall, 
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector, alphaDigitMaster, _ivrReport);
            _helper.LoadGrammar(GrammerRule.onedigit_voice);
            _helper.LoadGrammar(GrammerRule.LanguagePrompt);
           // _helper.LanguagePrompt();
            LanguagePrompt();
            _idleTimer.Start();
        }

        void LanguagePrompt()

        {
            try
            {
                _speechSynthesizer.Speak("Thank you for calling Atlas van lines");

                string voiceName = "Microsoft Server Speech Text to Speech Voice (es-MX, Hilda)";
                
                SpeechSynthesizer spanishSynthesizer = new SpeechSynthesizer();
                SpeechAudioFormatInfo audioformat = new SpeechAudioFormatInfo(16000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
                spanishSynthesizer.SetOutputToAudioStream(_speechSynthesisConnector, audioformat);
                spanishSynthesizer.SelectVoice(voiceName);
                spanishSynthesizer.Speak("Presione 2 para espanol");

                _speechRecognitionEngine.RecognizeAsync(RecognizeMode.Multiple);
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
            }
        }


        void avCall_StateChanged(object sender, CallStateChangedEventArgs e)
        {
            _logger.DebugFormat("Call State changed {0}", e.State);
            //throw new NotImplementedException();
        }

        #endregion

        #region Private Methods
        
        private AlphaDigitMapping  CreateAlphaDigitsDict(string alpha, string digit)
        {
            AlphaDigitMapping alphaDigitMapping = new AlphaDigitMapping();
            alphaDigitMapping.Alpha = alpha;
            alphaDigitMapping.Digit = digit;
            return alphaDigitMapping;
        }
        
        private void LoadData()
        {
             alphaDigitMaster = new Dictionary<string, List<AlphaDigitMapping>>();

            List<AlphaDigitMapping> alphaDigit2Mappings = new List<AlphaDigitMapping>();
            // Data for 2
            alphaDigit2Mappings.Add(CreateAlphaDigitsDict("A", "1"));
            alphaDigit2Mappings.Add(CreateAlphaDigitsDict("B", "2"));
            alphaDigit2Mappings.Add(CreateAlphaDigitsDict("C", "3"));

            alphaDigitMaster.Add("2", alphaDigit2Mappings);

            List<AlphaDigitMapping> alphaDigit3Mappings = new List<AlphaDigitMapping>();
            // Data for 3
            alphaDigit3Mappings.Add(CreateAlphaDigitsDict("D", "1"));
            alphaDigit3Mappings.Add(CreateAlphaDigitsDict("E", "2"));
            alphaDigit3Mappings.Add(CreateAlphaDigitsDict("F", "3"));

            alphaDigitMaster.Add("3", alphaDigit3Mappings);

            List<AlphaDigitMapping> alphaDigit4Mappings = new List<AlphaDigitMapping>();
            // Data for 4
            alphaDigit4Mappings.Add(CreateAlphaDigitsDict("G", "1"));
            alphaDigit4Mappings.Add(CreateAlphaDigitsDict("H", "2"));
            alphaDigit4Mappings.Add(CreateAlphaDigitsDict("I", "3"));

            alphaDigitMaster.Add("4", alphaDigit4Mappings);

            List<AlphaDigitMapping> alphaDigit5Mappings = new List<AlphaDigitMapping>();
            // Data for 5
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("J", "1"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("K", "2"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("L", "3"));

            alphaDigitMaster.Add("5", alphaDigit5Mappings);

            List<AlphaDigitMapping> alphaDigit6Mappings = new List<AlphaDigitMapping>();
            // Data for 6
            alphaDigit6Mappings.Add(CreateAlphaDigitsDict("M", "1"));
            alphaDigit6Mappings.Add(CreateAlphaDigitsDict("N", "2"));
            alphaDigit6Mappings.Add(CreateAlphaDigitsDict("O", "3"));

            alphaDigitMaster.Add("6", alphaDigit6Mappings);

            List<AlphaDigitMapping> alphaDigit7Mappings = new List<AlphaDigitMapping>();
            // Data for 7
            alphaDigit7Mappings.Add(CreateAlphaDigitsDict("P", "1"));
            alphaDigit7Mappings.Add(CreateAlphaDigitsDict("Q", "2"));
            alphaDigit7Mappings.Add(CreateAlphaDigitsDict("R", "3"));
            alphaDigit7Mappings.Add(CreateAlphaDigitsDict("S", "4"));

            alphaDigitMaster.Add("7", alphaDigit7Mappings);

            List<AlphaDigitMapping> alphaDigit8Mappings = new List<AlphaDigitMapping>();
            // Data for 8
            alphaDigit8Mappings.Add(CreateAlphaDigitsDict("T", "1"));
            alphaDigit8Mappings.Add(CreateAlphaDigitsDict("U", "2"));
            alphaDigit8Mappings.Add(CreateAlphaDigitsDict("V", "3"));

            alphaDigitMaster.Add("8", alphaDigit8Mappings);

            List<AlphaDigitMapping> alphaDigit9Mappings = new List<AlphaDigitMapping>();
            // Data for 9
            alphaDigit9Mappings.Add(CreateAlphaDigitsDict("W", "1"));
            alphaDigit9Mappings.Add(CreateAlphaDigitsDict("X", "2"));
            alphaDigit9Mappings.Add(CreateAlphaDigitsDict("Y", "3"));
            alphaDigit9Mappings.Add(CreateAlphaDigitsDict("Z", "4"));

            alphaDigitMaster.Add("9", alphaDigit9Mappings);
            
            promptAudioData.Add(IvrConstant.INITIAL_PROMPT_TIMER_ELAPSED, new PromptAudio() { PromptText = "I did not get this. Please make atleast one selection.", PromptAudioPath = "" });

            timerElapsedCounter.Add(CallWorkFlowState.InitialPrompt, 0);
            timerElapsedCounter.Add(CallWorkFlowState.First2Digits, 0);
            timerElapsedCounter.Add(CallWorkFlowState.AlphaDigits, 0);
            timerElapsedCounter.Add(CallWorkFlowState.Last5Digits, 0);

            _idleTimer = new System.Timers.Timer();
            _idleTimer.Elapsed += idleTimer_Elapsed;
            _idleTimer.Interval = 4000;
        }
        
        private void InitializeState(string cultureName)
        {
            if (cultureName == IvrConstant.CULTURE_NAME_SPANISH)
            {
                InitializeEngine(IvrConstant.CULTURE_NAME_SPANISH);
            }

            _ivrReport.Language = cultureName;
            
            _workFlow = new IvrWorkFlow(_speechSynthesizer, _speechRecognitionEngine, promptAudioData, _avCall,
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector, alphaDigitMaster, _ivrReport);
            _helper = new IvrHelper(_speechSynthesizer, _speechRecognitionEngine, _avCall,
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector, alphaDigitMaster, _ivrReport);

            _helper.LoadGrammar(GrammerRule.onedigit_voice);

            _helper.InitialPrompt();
          //  _idleTimer.Start();
        }

        private void InitializeEngine(string cultureName = "")
        {
            if (_speechRecognitionConnector.IsActive)
            {
                // Register for notification of the events on the speech synthesizer.
                _speechSynthesizer.StateChanged -= new EventHandler<StateChangedEventArgs>(synth_StateChanged);

                _speechRecognitionEngine.SpeechRecognized -= new EventHandler<SpeechRecognizedEventArgs>(speechRecognitionEngine_SpeechRecognized);
                _speechRecognitionEngine.SpeechDetected -= SpeechRecognitionEngine_SpeechDetected;
                _speechRecognitionEngine.SpeechRecognitionRejected -= SpeechRecognitionEngine_SpeechRecognitionRejected;
                //_speechRecognitionEngine.RecognizeCompleted -= SpeechRecognitionEngine_RecognizeCompleted;
                _speechRecognitionConnector.Stop();
            }

            _speechSynthesizer = new SpeechSynthesizer();
            _speechSynthesizer.Rate = 0;
            SpeechAudioFormatInfo audioformat = new SpeechAudioFormatInfo(16000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
            _speechSynthesizer.SetOutputToAudioStream(_speechSynthesisConnector, audioformat);

            var t = _speechSynthesizer.GetInstalledVoices();
            foreach (InstalledVoice voice in t)
            {
                _logger.DebugFormat("Voice Name is {0}",voice.VoiceInfo.Name);
            }

            if (cultureName == IvrConstant.CULTURE_NAME_SPANISH)
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                string voiceName = "Microsoft Server Speech Text to Speech Voice (es-MX, Hilda)";
              //  string voiceName = "VW James";
                _speechSynthesizer.SelectVoice(voiceName);
               
                _speechRecognitionEngine = new SpeechRecognitionEngine(new System.Globalization.CultureInfo(cultureName));
            }
            else
            {
               // string voiceName = "VW James";//"Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)";
                string voiceName = "Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)";
                _speechSynthesizer.SelectVoice(voiceName);
                _speechRecognitionEngine = new SpeechRecognitionEngine();
            }

            // Register for notification of the events on the speech synthesizer.
            _speechSynthesizer.StateChanged += new EventHandler<StateChangedEventArgs>(synth_StateChanged);

            _speechRecognitionEngine.SpeechRecognized += speechRecognitionEngine_SpeechRecognized;
            _speechRecognitionEngine.SpeechDetected += SpeechRecognitionEngine_SpeechDetected;
            _speechRecognitionEngine.SpeechRecognitionRejected += SpeechRecognitionEngine_SpeechRecognitionRejected;
            //_speechRecognitionEngine.RecognizeCompleted += SpeechRecognitionEngine_RecognizeCompleted;

            //Start recognizing
            SpeechRecognitionStream stream = _speechRecognitionConnector.Start();
            
            SpeechAudioFormatInfo speechAudioFormatInfo = new SpeechAudioFormatInfo(8000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
            _speechRecognitionEngine.SetInputToAudioStream(stream, speechAudioFormatInfo);

        }
        #endregion

        #region Timer
        private void ResetTimer()
        {
            _idleTimer.Stop();
            _idleTimer.Start();
        }

        void idleTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _idleTimer.Stop();

            if (currentState == CallWorkFlowState.LanguagePrompt)
            {
                _timeElapsedLanguagePrompt = true;
                InitializeState(IvrConstant.CULTURE_NAME_ENGLIGH);
                currentState = CallWorkFlowState.InitialPrompt;
                return;
            }

            var prompt = promptAudioData[IvrConstant.INITIAL_PROMPT_TIMER_ELAPSED];

          //  _helper.SpeakPromptAsync(prompt, false);

            switch (_workFlow.CurrentState)
            {
                case CallWorkFlowState.InitialPrompt:
                    timerElapsedCounter[CallWorkFlowState.InitialPrompt]++;
                    if (timerElapsedCounter[CallWorkFlowState.InitialPrompt] > 3)
                    {
                        _helper.DisconnectCall();
                        return;
                    }

                    break;
                case CallWorkFlowState.First2Digits:
                    //var prompt = promptAudioData[IvrConstant.INITIAL_PROMPT_TIMER_ELAPSED];
                    timerElapsedCounter[CallWorkFlowState.First2Digits]++;
                    if (timerElapsedCounter[CallWorkFlowState.First2Digits] > 3)
                    {
                        _helper.DisconnectCall();
                        return;
                    }
                    break;
                case CallWorkFlowState.AlphaDigits:
                    timerElapsedCounter[CallWorkFlowState.AlphaDigits]++;
                    if (timerElapsedCounter[CallWorkFlowState.AlphaDigits] > 3)
                    {
                        _helper.DisconnectCall();
                        return;
                    }
                    break;
                case CallWorkFlowState.Last5Digits:
                    break;
                default:
                    break;
            }

            //_idleTimer.Start();
        }

        #endregion

        #region Synthesizer Events

        // Write the current state of the synthesizer to the console.
        private void synth_StateChanged(object sender, StateChangedEventArgs e)
        {
            Console.WriteLine("Synthesizer State: " + e.State);
            _logger.DebugFormat("Synthesizer State: {0}", e.State);
        }

        #endregion

        #region DTMF Events
       
        private void ToneController_ToneReceived(object sender, ToneControllerEventArgs e)
        {
            try
            {
                //_dtmfRecognitionEngine.AddTone((byte)e.Tone);

                //Console.WriteLine("Tone Received: " + (ToneId)e.Tone + " (" + e.Tone + ")");
                _logger.DebugFormat("Tone Received: {0} ( {1} )", (ToneId)e.Tone, e.Tone);

                //_idleTimer.Stop();

                _speechSynthesizer.SpeakAsyncCancelAll();

                if (currentState == CallWorkFlowState.LanguagePrompt)
                {
                    if (_timeElapsedLanguagePrompt) return;

                    _idleTimer.Stop();
                    switch ((ToneId)e.Tone)
                    {
                        case ToneId.Tone1:
                            //_helper.LoadGrammar(GrammerRule.onedigit_voice);
                            InitializeState(IvrConstant.CULTURE_NAME_ENGLIGH);
                            currentState = CallWorkFlowState.InitialPrompt;
                            break;
                        case ToneId.Tone2:
                            //_helper.LoadGrammar(GrammerRule.onedigit_voice);
                            InitializeState(IvrConstant.CULTURE_NAME_SPANISH);
                            currentState = CallWorkFlowState.InitialPrompt;
                            break;
                        default:
                            _helper.TryAgain(true);
                            break;
                    }
                    return;
                }

                //if (currentState == CallWorkFlowState.InitialPrompt)
                //{
                //    // For the initial prompt; anything other than one or two is invalid
                //    if ((!e.Tone.Equals(ToneId.Tone1)) && (!e.Tone.Equals(ToneId.Tone2)))
                //    {
                //        _helper.InvalidSelection(false);
                //        _helper.InitialPrompt();
                //        return;
                //    }
                //}

                // If user pressed star, reinitialize
                if ((ToneId)e.Tone == ToneId.Star)
                {
                    InitializeState(_speechRecognitionEngine.RecognizerInfo.Culture.Name);
                    return;
                }

                UserInput userInput = new UserInput();
                userInput.IsDtmf = true;

                if (_workFlow.IsYesNoPrompt)
                {   
                    switch ((ToneId)e.Tone)
                    {
                        case ToneId.Pound:
                            userInput.Text = "yes";
                            _workFlow.HandleCallWorkFlow(userInput);
                            break;
                        case ToneId.Tone2:
                            userInput.Text = "no";
                            _workFlow.HandleCallWorkFlow(userInput);
                            break;
                        default:
                            _helper.TryAgain(true);
                            return;
                    }
                }
                else
                {
                    userInput.Text = e.Tone.ToString();
                    _workFlow.HandleToneWorkFlow(userInput);
                }

                //_idleTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
                _helper.DisconnectCall();
            }
        }

        #endregion

        #region Speech Recognition Events

        private void SpeechRecognitionEngine_SpeechDetected(object sender, SpeechDetectedEventArgs e)
        {
            _logger.Debug("Speech detected.");
        }

        private void SpeechRecognitionEngine_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            _logger.DebugFormat("Could not recognize");

            foreach (var item in e.Result.Words)
            {
                _logger.DebugFormat("Text: {0}", item.Text);
            }
        }

        private void SpeechRecognitionEngine_RecognizeCompleted(object sender, RecognizeCompletedEventArgs e)
        {
            try
            {
                _logger.Debug("recognize completed.");
                if (e.InitialSilenceTimeout)
                {
                    _silentAttempt++;
                    _logger.DebugFormat("Recognize InitialSilenceTimeout:{0}", e.InitialSilenceTimeout);
                    if (_silentAttempt > 3)
                    {
                        _helper.DisconnectingPrompt();
                        _helper.DisconnectCall();
                    }
                    else
                    {
                        _helper.TryAgain(true);
                    }
                }

                if (e.BabbleTimeout)
                {
                    _logger.DebugFormat("Recognize BabbleTimeout:{0}", e.BabbleTimeout);
                    _helper.DisconnectingPrompt();
                    _helper.DisconnectCall();
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
                _helper.DisconnectCall();
            }
        }

        private void speechRecognitionEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            try
            {
                _idleTimer.Stop();

                _speechSynthesizer.SpeakAsyncCancelAll();
                
                if (e.Result != null)
                {
                    if (currentState == CallWorkFlowState.LanguagePrompt)
                    {
                        _logger.DebugFormat("Text: {0}", e.Result.Text);

                        switch (e.Result.Semantics.Value.ToString())
                        {
                            case "one":
                                InitializeState(IvrConstant.CULTURE_NAME_ENGLIGH);
                                currentState = CallWorkFlowState.InitialPrompt;
                                break;
                            case "two":
                                InitializeState(IvrConstant.CULTURE_NAME_SPANISH);
                                currentState = CallWorkFlowState.InitialPrompt;
                                break;
                            default:
                                _helper.TryAgain(true);
                                break;
                        }
                        return;
                    }

                    //if (currentState == CallWorkFlowState.InitialPrompt)
                    //{
                    //    // For the initial prompt; anything other than one or two is invalid
                    //    if (e.Result.Semantics.Value.ToString().ToLower() != "1" && e.Result.Semantics.Value.ToString().ToLower() != "2")
                    //    {
                    //        _helper.InvalidSelection(false);
                    //        _helper.InitialPrompt();
                    //        return;
                    //    }
                    //}

                    if (e.Result.Semantics.Value.ToString() == "star")
                    {
                        InitializeState(_speechRecognitionEngine.RecognizerInfo.Culture.Name);
                        return;
                    }

                    _logger.DebugFormat("Current call work flow state:{0}", _workFlow.CurrentState);
                    _logger.DebugFormat("Text: {0}", e.Result.Semantics.Value.ToString());

                    UserInput userInput = new UserInput();
                    userInput.Text = e.Result.Semantics.Value.ToString();

                    _workFlow.HandleCallWorkFlow(userInput);
                }
                else
                {
                    //If neither grammar was used, this is not recognized speech.
                    //Console.WriteLine(string.Format("Could not recognize: {0} ", e.Result));
                    _logger.DebugFormat("Could not recognize: {0} ", e.Result);
                }

                // _waitForRecoCompleted.Set();
              //  _idleTimer.Start();
            }
            catch(Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
                _helper.DisconnectCall();
            }
        }

        #endregion

        private void OnFlowStateChanged(object sender, MediaFlowStateChangedEventArgs e)
        {
            try
            {
                //_waitForCallToBeAccepted.Set();
                _logger.InfoFormat("State changed from '{0}' to '{1}'", e.PreviousState, e.State);

                if (e.State == MediaFlowState.Terminated)
                {
                    _avCall.Flow.StateChanged -= new EventHandler<MediaFlowStateChangedEventArgs>(OnFlowStateChanged);
                    // If (ivr report is not null, save data
                    if (_ivrReport != null)
                    {
                        _helper.DisconnectCall();
                    }
                    _speechRecognitionConnector.Stop();
                    _speechRecognitionConnector.DetachFlow();

                    _speechSynthesisConnector.Stop();
                    _speechSynthesisConnector.DetachFlow();
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
            }
        }
    }
}

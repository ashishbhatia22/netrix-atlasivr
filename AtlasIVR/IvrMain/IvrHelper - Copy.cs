﻿using log4net;
using Microsoft.Rtc.Collaboration;
using Microsoft.Rtc.Collaboration.AudioVideo;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.Synthesis;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Text;
using System.Xml.Serialization;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class IvrHelper
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IvrHelper));
        private SpeechSynthesizer _speechSynthesizer;
        private SpeechRecognitionEngine _speechRecognitionEngine;
        private AudioVideoCall _avCall;
        private bool recognitionCompleted = false;
        private AutoResetEvent _waitForRecoCompleted;
        private DtmfRecognitionEngine _dtmfRecognitionEngine;
        private SpeechRecognitionConnector _speechRecognitionConnector;
        private SpeechSynthesisConnector _speechSynthesisConnector;
        private PromptData _promptData;
        Dictionary<string, List<AlphaDigitMapping>> _alphaDigitMaster;
        public IvrHelper(SpeechSynthesizer synthesizer, SpeechRecognitionEngine recognitionEngine,             
            AudioVideoCall avCall, AutoResetEvent recoEvent,
            DtmfRecognitionEngine dtmfRecognitionEngine,
            SpeechRecognitionConnector speechRecognitionConnector,
            SpeechSynthesisConnector speechSynthesisConnector,
           Dictionary<string, List<AlphaDigitMapping>> alphaDigitMaster)
        {
            _speechRecognitionEngine = recognitionEngine;
            _speechSynthesizer = synthesizer;
            _avCall = avCall;
            _waitForRecoCompleted = recoEvent;
            _dtmfRecognitionEngine = dtmfRecognitionEngine;
            _speechRecognitionConnector = speechRecognitionConnector;
            _speechSynthesisConnector = speechSynthesisConnector;
            _alphaDigitMaster = alphaDigitMaster;
            LoadPromptData();
        }

        private void LoadPromptData()
        {
            XmlSerializer xs = new XmlSerializer(typeof(PromptData));
            string filePath = string.Empty;

            if (_speechRecognitionEngine.RecognizerInfo.Culture.Name == IvrConstant.CULTURE_NAME_SPANISH)
            {
                filePath = "Spanish.xml";
            }
            else
            {
                filePath = "English.xml";
            }

            var fileStream = new FileStream(filePath, FileMode.Open);

            _promptData = (PromptData)xs.Deserialize(fileStream);

            fileStream.Dispose();
        }

        public void AskLast5Digits(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.Last5Numbers, PromptAudioPath = "006.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void AskState(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = "Please say or enter a state.", PromptAudioPath = "state.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void ReferenceNumberNotFound(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.ReferenceNumberNotFound, PromptAudioPath = "NoReferenceNumberfound.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPrompt(promptData, recognize);
        }

        public void ReferenceNumberPending(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.ReferenceNumberPending, PromptAudioPath = "Pending.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPrompt(promptData, recognize);
        }

        public void TransferCall(string name, bool recognize = true)
        {
            //PromptBuilder builder = new PromptBuilder();
            //builder.AppendText("You will now be forwarded to your atlas representative ");
            //builder.AppendText(name);
            //_speechSynthesizer.Speak(builder);
            var promptData = new PromptAudio() { PromptText = string.Format("{0} {1}", _promptData.FinalConfirmation, name), PromptAudioPath = "Transfer.wav" };
            SpeakPrompt(promptData, recognize);
        }

        public void AskCity(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = "Please say or enter the destination city.", PromptAudioPath = "City.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }


        public void AskAlphaDigits(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.AlphaCharacters, PromptAudioPath = "005.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void PromptConfirmation(string resultCapture, bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = String.Format("{0}, {1} {2}.",_promptData.Confirmation, resultCapture, _promptData.PoundConfirmation), PromptAudioPath = "" };
            SpeakPromptAsync(promptData, recognize);
        }

        public void PromptAlphaDigitConfirmation(string resultCapture)
        {
            // look in dictionary and then create the prompt;
            List<AlphaDigitMapping> alphaDigitMapper = _alphaDigitMaster[resultCapture];

            string promptText = String.Format("Press 1 for {0} Press 2 for {1} Press 3 for {2}", alphaDigitMapper[0].Alpha, alphaDigitMapper[1].Alpha, alphaDigitMapper[2].Alpha);

            var promptData = new PromptAudio() { PromptText = promptText, PromptAudioPath = "" };
            SpeakPromptAsync(promptData, true);
        }

        public void PromptAlphaDigitDTMF(int digitOrder)
        {
            string promptText = String.Empty;

            if (digitOrder.Equals(1))
            {
                promptText = "Please enter the first digit of alpha reference number";
            }
            else
            {
                promptText = "Please enter the second digit of alpha reference number";
            }

            var promptData = new PromptAudio() { PromptText = promptText, PromptAudioPath = "" };
            SpeakPromptAsync(promptData, true);
        }


        public void PromptConfirmationWithDelay(List<string> resultCapture, bool recognize = true)
        {
            try
            {
                PromptBuilder builder = new PromptBuilder();
                builder.AppendText(_promptData.Confirmation + ",");
                foreach (string word in resultCapture)
                {
                    builder.AppendBreak(new TimeSpan(5000));
                    builder.AppendText(word);
                }

                builder.AppendBreak(new TimeSpan(5000));
                builder.AppendText(_promptData.PoundConfirmation);

                _speechSynthesizer.Speak(builder);

                if (recognize)
                {
                    RecognizeName();
                }
            }
            catch (Exception ex)
            {
                // TODO: log exception
            }
        }


        //public void PromptFirst2DigitInValid(string resultCapture, bool recognize = true)
        //{
        //    var promptData = new PromptAudio() { PromptText = "I did not get that. Please say or enter only digits", PromptAudioPath = "" };
        //    // Console.WriteLine("SynthName()");
        //    SpeakPromptAsync(promptData, recognize);

        //}

        public void AskFirst2Digits(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.First2Digits, PromptAudioPath = "003.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void AskUserToWait()
        {
            var promptData = new PromptAudio() { PromptText = "Please wait for reference number email. Once you get the email, please try calling again", PromptAudioPath = "NoReferenceNumber.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPrompt(promptData, false);
            // Disconnect call
            DisconnectCall();
        }

        public void DisconnectingPrompt()
        {
            var promptData = new PromptAudio() { PromptText = "Disconnecting" };
            // Console.WriteLine("SynthName()");
            SpeakPrompt(promptData, false);
        }

        public void TryAgain(bool recognize)
        {
            var promptData = new PromptAudio() { PromptText = string.Format("{0}{1}", _promptData.DidNotUnderstand, _promptData.TryAgain) };
            SpeakPromptAsync(promptData, recognize);
        }

        public void LanguagePrompt()
        {
            //var promptData = new PromptAudio() { PromptText = "Hola..." };
            var promptData = new PromptAudio() { PromptText = "Press 1, for English, Press 2, for Spanish." };
            SpeakPromptAsync(promptData, true);
        }

        public void SpanishNotReady(bool recognize)
        {
            var promptData = new PromptAudio() { PromptText = "We are working on Spanish... Please, press 1 for English." };
            SpeakPromptAsync(promptData, recognize);
        }

        public void InitialPrompt()
        {
            var promptData = new PromptAudio() { PromptText = _promptData.InitialPrompt };
            SpeakPromptAsync(promptData, true);
        }


        public void SpeakPromptAsync(PromptAudio input, bool recognize = true)
        {
            try
            {
                // temporarily set the audio path to null to have a consistent user experience
                input.PromptAudioPath = null;

                if (string.IsNullOrEmpty(input.PromptAudioPath))
                {
                    PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                    builder.AppendText(input.PromptText);
                    _speechSynthesizer.SpeakAsync(builder);
                   // _speechSynthesizer.SpeakAsync(input.PromptText);
                }
                else
                {
                    const string path = @"C:\atlasIVR - Copy\AtlasIVR\Prompts";

                    PromptBuilder builder = new PromptBuilder();
                    builder.AppendAudio(Path.Combine(path, input.PromptAudioPath));
                    builder.AppendBreak(new TimeSpan(500));
                    // builder.AppendAudio(@"C:\atlasIVR - Copy\AtlasIVR\Prompts\002.wav");
                    _speechSynthesizer.SpeakAsync(builder);
                }

                if (recognize)
                {
                    RecognizeName();
                }
            }
            //catch (System.IO.EndOfStreamException)
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
                //Eat this exception..
            }

        }

        public void SpeakPrompt(PromptAudio input, bool recognize = true)
        {
            try
            {
                // temporarily set the audio path to null to have a consistent user experience
                input.PromptAudioPath = null;
                if (string.IsNullOrEmpty(input.PromptAudioPath))
                {
                    PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                    builder.AppendText(input.PromptText);
                    _speechSynthesizer.Speak(builder);
                   // _speechSynthesizer.Speak(input.PromptText);
                }
                else
                {
                    const string path = @"C:\atlasIVR - Copy\AtlasIVR\Prompts";

                    PromptBuilder builder = new PromptBuilder();
                    
                    builder.AppendAudio(Path.Combine(path, input.PromptAudioPath));
                    builder.AppendBreak(new TimeSpan(500));
                    
                    // builder.AppendAudio(@"C:\atlasIVR - Copy\AtlasIVR\Prompts\002.wav");
                    _speechSynthesizer.Speak(builder);
                }

                if (recognize)
                {
                    RecognizeName();
                }
            }
            //catch (System.IO.EndOfStreamException ex)
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
                //Eat this exception..
            }
        }

        public void RecognizeName()
        {
            Console.WriteLine("\r\nRecognizing...");
            _logger.Debug("Recognizing...");
            try
            {
                //_speechRecognitionEngine.InitialSilenceTimeout = new TimeSpan(0, 0, 5);
                //_speechRecognitionEngine.BabbleTimeout = new TimeSpan(0, 0, 30);
                //_speechRecognitionEngine.EndSilenceTimeout = new TimeSpan(0, 0, 5);

                _speechRecognitionEngine.RecognizeAsync(RecognizeMode.Multiple);

                _dtmfRecognitionEngine.RecognizeAsync();
               // _waitForRecoCompleted.WaitOne();
            }
            catch (Exception ex)
            {
                //_logger.Error(ex);
                //Console.WriteLine(ex.Message);
            }
        }

        public void TransferCall(string transferToNumber)
        {
            CallTransferOptions basicTransfer = new CallTransferOptions(CallTransferType.Unattended);

            _avCall.BeginTransfer(transferToNumber, basicTransfer, EndTransfer, null);

        }

        private void EndTransfer(IAsyncResult ar)
        {
            try
            {
                _avCall.EndTransfer(ar);
            }
            catch (Exception ex)
            {

            }
            //  throw new NotImplementedException();
        }

        public void DisconnectCall()
        {
            try
            {
                //DisconnectingPrompt();
                _avCall.BeginTerminate(EndTerminateCall, _avCall);
            }
            catch(Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
            }
        }

        private void EndTerminateCall(IAsyncResult ar)
        {
            try
            {
                AudioVideoCall AVCall = ar.AsyncState as AudioVideoCall;

                // Complete the termination of the incoming call.
                AVCall.EndTerminate(ar);

                Console.WriteLine("Terminated the call...");
                _logger.Debug("Terminated the call...");

                _speechRecognitionConnector.Stop();
                _speechRecognitionConnector.DetachFlow();

                _speechSynthesisConnector.Stop();
                _speechSynthesisConnector.DetachFlow();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
            }
        }


        public void LoadCityGrammar()
        {
            //if (!_speechRecognitionEngine.Grammars.Any(x => x.Name == ruleName))
            //{
                _speechRecognitionEngine.UnloadAllGrammars();
                _logger.DebugFormat("Unloaded all grammers.");
            //}
           // Choices citiesGrammar = new Choices();
            GrammarBuilder grammarBuilder = new GrammarBuilder();

            string cGrammar = String.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (string city in Cities.cities)
            {
               string c = String.Format("<item><item>{0}</item><tag>$.value='>{0}{1}", city, city);
               sb.Append(c);
                SemanticResultValue temp = new SemanticResultValue(city,city);
               //. citiesGrammar.Add(temp);
                grammarBuilder.Append(temp);
                cGrammar = cGrammar + c;
            }

            Grammar citiesGrammar = new Grammar(grammarBuilder);
            citiesGrammar.Name = "Cities";
            _speechRecognitionEngine.LoadGrammar(citiesGrammar);

            //_speechRecognitionEngine.Grammars[4].Name = "Cities";
            //Choices citiesGrammar = new Choices(city);
            //_speechRecognitionEngine.LoadGrammar(new Grammar(new GrammarBuilder(citiesGrammar, 1, 1)));
            //_speechRecognitionEngine.Grammars[4].Name = "Cities";
        }

        public void LoadGrammar(GrammerRule ruleName)
        {
            string fileName = string.Empty;
            string dtmfFileName = string.Empty;
            Grammar dtmfGrammar = null;
            
            _speechRecognitionEngine.UnloadAllGrammars();
            _logger.DebugFormat("Unloaded all grammers.");

            switch (ruleName)
            {
                case GrammerRule.onedigit_voice:
                    fileName = "OneDigitVoice.grxml";
                    dtmfGrammar = new Grammar(@"Grammars\Dtmf\OneDigitDtmf.grxml", "onedigit_dtmf");
                    break;
                case GrammerRule.twodigits_voice:
                    fileName = "TwoDigitsVoice.grxml";
                    dtmfGrammar = new Grammar(@"Grammars\Dtmf\TwoDigitsDtmf.grxml", "twodigits_dtmf");
                    break;
                case GrammerRule.alphadigits_voice:
                    fileName = "AlphaDigitsVoice.grxml";
                    dtmfGrammar = new Grammar(@"Grammars\Dtmf\OneDigitDtmf.grxml", "onedigit_dtmf");
                    break;
                case GrammerRule.fivedigits_voice:
                    fileName = "FiveDigitsVoice.grxml";
                    dtmfGrammar = new Grammar(@"Grammars\Dtmf\FiveDigitsDtmf.grxml", "fivedigits_dtmf");
                    break;
                case GrammerRule.state_voice:
                    fileName = "StateVoice.grxml";
                    break;
                case GrammerRule.city_voice:
                    fileName = "CityVoice.grxml";
                    break;
                case GrammerRule.Confirmation_Voice:
                    fileName = "ConfirmationVoice.grxml";
                    break;
                default:
                    break;
            }

            Grammar voiceGrammar;

            if (_speechRecognitionEngine.RecognizerInfo.Culture.Name == IvrConstant.CULTURE_NAME_SPANISH)
            {
                voiceGrammar = new Grammar(@"Grammars\Spanish\" + fileName, ruleName.ToString());
            }
            else
            {
                voiceGrammar = new Grammar(@"Grammars\" + fileName, ruleName.ToString());
            }

            voiceGrammar.Name = ruleName.ToString();
            _speechRecognitionEngine.LoadGrammar(voiceGrammar);
            _logger.DebugFormat("Loaded {0} voice grammer.", ruleName);

            if (dtmfGrammar != null)
            {
                _dtmfRecognitionEngine.LoadGrammarAsync(dtmfGrammar);
                _logger.DebugFormat("Loaded {0} dtmf grammer.", dtmfGrammar.RuleName);
            }
        }
    }
}

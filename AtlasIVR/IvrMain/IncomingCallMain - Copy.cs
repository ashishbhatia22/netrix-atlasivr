﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Rtc.Collaboration.AudioVideo;

using Microsoft.Speech.Recognition;
using Microsoft.Speech.Synthesis;
using Microsoft.Speech.AudioFormat;
using log4net;
using System.Threading;
using Timers = System.Timers;
using System.Speech;
using Microsoft.Rtc.Collaboration;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class IncomingCallMain
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IncomingCallMain));
        //  private AudioVideoFlow _audioVideoFlow;
        private SpeechSynthesisConnector _speechSynthesisConnector;
        private SpeechSynthesizer _speechSynthesizer;
        private SpeechRecognitionEngine _speechRecognitionEngine;
        private SpeechRecognitionConnector _speechRecognitionConnector;
        private DtmfRecognitionEngine _dtmfRecognitionEngine;
        private AudioVideoCall _avCall;
        private AudioVideoFlow _avCallFlow;
        private CallWorkFlowState currentState = CallWorkFlowState.LanguagePrompt;
        private Dictionary<string, PromptAudio> promptAudioData = new Dictionary<string, PromptAudio>();
        private Dictionary<CallWorkFlowState, int> timerElapsedCounter = new Dictionary<CallWorkFlowState, int>();
        private Timers.Timer _idleTimer;
        private IvrWorkFlow _workFlow;
        private IvrHelper _helper;
        private AutoResetEvent _waitForRecoCompleted = new AutoResetEvent(false);
        private AutoResetEvent waitForAudioVideoFlowStateChangedToActiveCompleted = new AutoResetEvent(false);
        private AutoResetEvent waitForAudioVideoCallEstablished = new AutoResetEvent(false); 
        private AutoResetEvent[] waitToProcessCall;
        private int _silentAttempt = 0;
        Dictionary<string, List<AlphaDigitMapping>> alphaDigitMaster;

        public IncomingCallMain()
        {
            LoadData();
        }
        
        #region Public Methods

        public void PromptUserForInput(AudioVideoCall avCall)
        {
            // Create AudioVideoFlow
            // _audioVideoFlow = _avCall;
            _avCall = avCall;
            _speechRecognitionConnector = new SpeechRecognitionConnector();
            _speechRecognitionConnector.AttachFlow(avCall.Flow);
            // Create a speech synthesis connector and attach the AudioVideoFlow instance to it.
            _speechSynthesisConnector = new SpeechSynthesisConnector();

            _speechSynthesisConnector.AttachFlow(avCall.Flow);

            _speechSynthesizer = new SpeechSynthesizer();
            _speechSynthesizer.Rate = 0;
            SpeechAudioFormatInfo audioformat = new SpeechAudioFormatInfo(16000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
            _speechSynthesizer.SetOutputToAudioStream(_speechSynthesisConnector, audioformat);

            //SpeechLib.SpVoice sp = new SpeechLib.SpVoice();
            //foreach (SpeechLib.ISpeechObjectToken token in sp.GetVoices(string.Empty,string.Empty))
            //{

            //}

            var t = _speechSynthesizer.GetInstalledVoices();
            //string voiceName = "Microsoft Server Speech Text to Speech Voice (es-ES, Helena)";
            string voiceName = "VW James";//"Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)";
            _speechSynthesizer.SelectVoice(voiceName); 
            // = _speechSynthesizer.GetInstalledVoices().Where(i => i.VoiceInfo.Name.Equals(s)).FirstOrDefault().VoiceInfo;
            
            //.VoiceInfo.Gender = VoiceGender.Male;
            // Register for notification of the events on the speech synthesizer.
            _speechSynthesizer.StateChanged += new EventHandler<StateChangedEventArgs>(synth_StateChanged);
            
            _speechSynthesisConnector.Start();
            //Start recognizing
            SpeechRecognitionStream stream = _speechRecognitionConnector.Start();
            var t1 = SpeechRecognitionEngine.InstalledRecognizers();
            _speechRecognitionEngine = new SpeechRecognitionEngine();
         
            //_speechRecognitionEngine.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(speechRecognitionEngine_SpeechRecognized);
            //_speechRecognitionEngine.SpeechDetected += SpeechRecognitionEngine_SpeechDetected;
            //_speechRecognitionEngine.SpeechRecognitionRejected += SpeechRecognitionEngine_SpeechRecognitionRejected;
            ////_speechRecognitionEngine.RecognizeCompleted += SpeechRecognitionEngine_RecognizeCompleted;

            SpeechAudioFormatInfo speechAudioFormatInfo = new SpeechAudioFormatInfo(8000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
            _speechRecognitionEngine.SetInputToAudioStream(stream, speechAudioFormatInfo);

            _dtmfRecognitionEngine = new DtmfRecognitionEngine();
            _dtmfRecognitionEngine.DtmfRecognized += DtmfRecognitionEngine_DtmfRecognized;
            _dtmfRecognitionEngine.DtmfRecognitionRejected += DtmfRecognitionEngine_DtmfRecognitionRejected;
            _dtmfRecognitionEngine.LoadGrammarCompleted += DtmfRecognitionEngine_LoadGrammarCompleted;
            _dtmfRecognitionEngine.RecognizeCompleted += DtmfRecognitionEngine_RecognizeCompleted;

            // Create a ToneController and attach to AVFlow
            ToneController toneController = new ToneController();
            toneController.AttachFlow(avCall.Flow);
            // Subscribe to callback to receive tones
            toneController.ToneReceived += new EventHandler<ToneControllerEventArgs>(ToneController_ToneReceived);

            _helper = new IvrHelper(_speechSynthesizer, _speechRecognitionEngine, _avCall, 
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector, alphaDigitMaster);
            _helper.LoadGrammar(GrammerRule.onedigit_voice);
            //helper.LoadGrammar("TwoDigitsDtmf.grxml", "twodigits_dtmf");

            _helper.LanguagePrompt();
        }

        #endregion

        #region Private Methods

        private void ProcessCall(object state)
        {
            WaitHandle.WaitAll(waitToProcessCall);
            CloseResetEvents();

            LoadData();
            InitializeEngine();

            //Prompt for English / Spanish
            _helper = new IvrHelper(_speechSynthesizer, _speechRecognitionEngine, _avCall,
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector,alphaDigitMaster);
            _helper.LoadGrammar(GrammerRule.onedigit_voice);
            _helper.LanguagePrompt();
        }

        private AlphaDigitMapping  CreateAlphaDigitsDict(string alpha, string digit)
        {
            AlphaDigitMapping alphaDigitMapping = new AlphaDigitMapping();
            alphaDigitMapping.Alpha = alpha;
            alphaDigitMapping.Digit = digit;
            return alphaDigitMapping;
        }



        private void LoadData()
        {
             alphaDigitMaster = new Dictionary<string, List<AlphaDigitMapping>>();
            List<AlphaDigitMapping> alphaDigit2Mappings = new List<AlphaDigitMapping>();
            
            // Data for 2
            alphaDigit2Mappings.Add(CreateAlphaDigitsDict("A", "1"));
            alphaDigit2Mappings.Add(CreateAlphaDigitsDict("B", "2"));
            alphaDigit2Mappings.Add(CreateAlphaDigitsDict("C", "3"));

            alphaDigitMaster.Add("2", alphaDigit2Mappings);

            List<AlphaDigitMapping> alphaDigit3Mappings = new List<AlphaDigitMapping>();
            
            // Data for 3
            alphaDigit3Mappings.Add(CreateAlphaDigitsDict("D", "1"));
            alphaDigit3Mappings.Add(CreateAlphaDigitsDict("E", "2"));
            alphaDigit3Mappings.Add(CreateAlphaDigitsDict("F", "3"));

            alphaDigitMaster.Add("3", alphaDigit3Mappings);

            List<AlphaDigitMapping> alphaDigit4Mappings = new List<AlphaDigitMapping>();
            

            // Data for 4
            alphaDigit4Mappings.Add(CreateAlphaDigitsDict("G", "1"));
            alphaDigit4Mappings.Add(CreateAlphaDigitsDict("H", "2"));
            alphaDigit4Mappings.Add(CreateAlphaDigitsDict("I", "3"));

            alphaDigitMaster.Add("4", alphaDigit4Mappings);

            List<AlphaDigitMapping> alphaDigit5Mappings = new List<AlphaDigitMapping>();
            

            // Data for 5
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("J", "1"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("K", "2"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("L", "3"));

            alphaDigitMaster.Add("5", alphaDigit5Mappings);

            // Data for 6
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("M", "1"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("N", "2"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("O", "3"));

            alphaDigitMaster.Add("6", alphaDigit5Mappings);

            // Data for 7
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("P", "1"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("Q", "2"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("R", "3"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("S", "4"));

            alphaDigitMaster.Add("7", alphaDigit5Mappings);

            // Data for 8
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("T", "1"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("U", "2"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("V", "3"));

            alphaDigitMaster.Add("8", alphaDigit5Mappings);

            // Data for 9
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("W", "1"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("X", "2"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("Y", "3"));
            alphaDigit5Mappings.Add(CreateAlphaDigitsDict("Z", "4"));

            alphaDigitMaster.Add("9", alphaDigit5Mappings);

            promptAudioData.Add(IvrConstant.INITIAL_PROMPT_TIMER_ELAPSED, new PromptAudio() { PromptText = "I did not get this. Please make atleast one selection.", PromptAudioPath = "" });

            timerElapsedCounter.Add(CallWorkFlowState.InitialPrompt, 0);
            timerElapsedCounter.Add(CallWorkFlowState.First2Digits, 0);
            timerElapsedCounter.Add(CallWorkFlowState.AlphaDigits, 0);
            timerElapsedCounter.Add(CallWorkFlowState.Last5Digits, 0);

            _idleTimer = new System.Timers.Timer();
            //_idleTimer.Elapsed += idleTimer_Elapsed;
            //_idleTimer.Interval = 25000;
        }

        private void InitializeEngine()
        {
            _speechRecognitionConnector = new SpeechRecognitionConnector();
            _speechRecognitionConnector.AttachFlow(_avCallFlow);

            // Create a speech synthesis connector and attach the AudioVideoFlow instance to it.
            _speechSynthesisConnector = new SpeechSynthesisConnector();
            _speechSynthesisConnector.AttachFlow(_avCallFlow);

            _speechSynthesizer = new SpeechSynthesizer();
            _speechSynthesizer.Rate = 0;
            SpeechAudioFormatInfo audioformat = new SpeechAudioFormatInfo(16000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
            _speechSynthesizer.SetOutputToAudioStream(_speechSynthesisConnector, audioformat);
            
            var t = _speechSynthesizer.GetInstalledVoices();
            //string voiceName = "Microsoft Server Speech Text to Speech Voice (es-ES, Helena)";
            string voiceName = "VW James";//"Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)";
            _speechSynthesizer.SelectVoice(voiceName);
            
            // Register for notification of the events on the speech synthesizer.
            _speechSynthesizer.StateChanged += new EventHandler<StateChangedEventArgs>(synth_StateChanged);
            _speechSynthesisConnector.Start();

            //Start recognizing
            SpeechRecognitionStream stream = _speechRecognitionConnector.Start();
            var t1 = SpeechRecognitionEngine.InstalledRecognizers();
            _speechRecognitionEngine = new SpeechRecognitionEngine();
            _speechRecognitionEngine.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(speechRecognitionEngine_SpeechRecognized);
            _speechRecognitionEngine.SpeechDetected += SpeechRecognitionEngine_SpeechDetected;
            _speechRecognitionEngine.SpeechRecognitionRejected += SpeechRecognitionEngine_SpeechRecognitionRejected;
            //_speechRecognitionEngine.RecognizeCompleted += SpeechRecognitionEngine_RecognizeCompleted;

            SpeechAudioFormatInfo speechAudioFormatInfo = new SpeechAudioFormatInfo(8000, AudioBitsPerSample.Sixteen, Microsoft.Speech.AudioFormat.AudioChannel.Mono);
            _speechRecognitionEngine.SetInputToAudioStream(stream, speechAudioFormatInfo);

            _dtmfRecognitionEngine = new DtmfRecognitionEngine();
            _dtmfRecognitionEngine.DtmfRecognized += DtmfRecognitionEngine_DtmfRecognized;
            _dtmfRecognitionEngine.DtmfRecognitionRejected += DtmfRecognitionEngine_DtmfRecognitionRejected;
            _dtmfRecognitionEngine.LoadGrammarCompleted += DtmfRecognitionEngine_LoadGrammarCompleted;
            _dtmfRecognitionEngine.RecognizeCompleted += DtmfRecognitionEngine_RecognizeCompleted;

            // Create a ToneController and attach to AVFlow
            ToneController toneController = new ToneController();
            toneController.AttachFlow(_avCallFlow);
            // Subscribe to callback to receive tones
            toneController.ToneReceived += new EventHandler<ToneControllerEventArgs>(ToneController_ToneReceived);
        }

        private void InitializeState(string cultureName)
        {
            if (cultureName == IvrConstant.CULTURE_NAME_SPANISH)
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                _speechRecognitionEngine = new SpeechRecognitionEngine(new System.Globalization.CultureInfo(cultureName));
                var t = _speechSynthesizer.GetInstalledVoices();
                string voiceName = "Microsoft Server Speech Text to Speech Voice (es-ES, Helena)";
                _speechSynthesizer.SelectVoice(voiceName); 
            }
            
            _workFlow = new IvrWorkFlow(_speechSynthesizer, _speechRecognitionEngine, promptAudioData, _avCall,
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector, alphaDigitMaster);
            _helper = new IvrHelper(_speechSynthesizer, _speechRecognitionEngine, _avCall,
                            _waitForRecoCompleted, _dtmfRecognitionEngine,
                            _speechRecognitionConnector, _speechSynthesisConnector, alphaDigitMaster);
            
            _helper.LoadGrammar(GrammerRule.onedigit_voice);
            _helper.InitialPrompt();

            _idleTimer.Start();
        }

        #endregion

        #region Timer
        private void ResetTimer()
        {
            _idleTimer.Stop();
            _idleTimer.Start();
        }

        void idleTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _idleTimer.Stop();

            var prompt = promptAudioData[IvrConstant.INITIAL_PROMPT_TIMER_ELAPSED];

            _helper.SpeakPromptAsync(prompt, false);

            switch (_workFlow.CurrentState)
            {
                case CallWorkFlowState.InitialPrompt:
                    timerElapsedCounter[CallWorkFlowState.InitialPrompt]++;
                    if (timerElapsedCounter[CallWorkFlowState.InitialPrompt] > 3)
                    {
                        _helper.DisconnectCall();
                        return;
                    }

                    break;
                case CallWorkFlowState.First2Digits:
                    //var prompt = promptAudioData[IvrConstant.INITIAL_PROMPT_TIMER_ELAPSED];
                    timerElapsedCounter[CallWorkFlowState.First2Digits]++;
                    if (timerElapsedCounter[CallWorkFlowState.First2Digits] > 3)
                    {
                        _helper.DisconnectCall();
                        return;
                    }
                    break;
                case CallWorkFlowState.AlphaDigits:
                    timerElapsedCounter[CallWorkFlowState.AlphaDigits]++;
                    if (timerElapsedCounter[CallWorkFlowState.AlphaDigits] > 3)
                    {
                        _helper.DisconnectCall();
                        return;
                    }
                    break;
                case CallWorkFlowState.Last5Digits:
                    break;
                default:
                    break;
            }

            _idleTimer.Start();
        }

        #endregion

        #region Synthesizer Events

        // Write the current state of the synthesizer to the console.
        private void synth_StateChanged(object sender, StateChangedEventArgs e)
        {
            Console.WriteLine("Synthesizer State: " + e.State);
            _logger.DebugFormat("Synthesizer State: {0}", e.State);
        }

        #endregion

        #region DTMF Events
        private void DtmfRecognitionEngine_RecognizeCompleted(object sender, DtmfRecognizeCompletedEventArgs e)
        {
            _dtmfRecognitionEngine.RecognizeAsync();
        }
        
        private void DtmfRecognitionEngine_LoadGrammarCompleted(object sender, LoadGrammarCompletedEventArgs e)
        {
            //_dtmfRecognitionEngine.RecognizeAsync();
        }

        private void DtmfRecognitionEngine_DtmfRecognitionRejected(object sender, DtmfRecognitionRejectedEventArgs e)
        {
            _logger.DebugFormat("Dtmf rejected");

            if (e.Result!=null)
            {
                _logger.DebugFormat("Dtmf rejected value:{0}", e.Result.Text);
            }

            if (_tonesReceived > 0)
            {
                _tonesReceived = _tonesReceived - 1;
            }
        }

        private void DtmfRecognitionEngine_DtmfRecognized(object sender, DtmfRecognizedEventArgs e)
        {
            _logger.DebugFormat("Dtmf recognized:{0}", e.Result.Semantics.Value);

            if (!_workFlow.IsYesNoPrompt)
            {
                UserInput userInput = new UserInput();
                userInput.IsDtmf = true;
                userInput.Text = e.Result.Semantics.Value.ToString();
                if (_workFlow.CurrentState==CallWorkFlowState.AlphaDigits)
                {
                    _workFlow.HandleToneWorkFlow(userInput);
                }
                else
                {
                    _workFlow.HandleCallWorkFlow(userInput);
                }
                _tonesReceived = 0;
            }
        }

        int _tonesReceived = 0;

        private void ToneController_ToneReceived(object sender, ToneControllerEventArgs e)
        {
            try
            {
                //Console.WriteLine("Tone Received: " + (ToneId)e.Tone + " (" + e.Tone + ")");
                _logger.DebugFormat("Tone Received: {0} ( {1} )", (ToneId)e.Tone, e.Tone);

                //_speechSynthesizer.SpeakAsyncCancelAll();
                
                _idleTimer.Stop();

                if (currentState == CallWorkFlowState.LanguagePrompt)
                {
                    switch ((ToneId)e.Tone)
                    {
                        case ToneId.Tone1:
                            _speechSynthesizer.SpeakAsyncCancelAll();
                            InitializeState(IvrConstant.CULTURE_NAME_ENGLIGH);
                            currentState = CallWorkFlowState.InitialPrompt;
                            break;
                        case ToneId.Tone2:
                            _speechSynthesizer.SpeakAsyncCancelAll();
                            InitializeState(IvrConstant.CULTURE_NAME_SPANISH);
                            currentState = CallWorkFlowState.InitialPrompt;
                            break;
                        default:
                            _helper.TryAgain(true);
                            break;
                    }
                    return;
                }

                // If user pressed star, reinitialize
                if ((ToneId)e.Tone == ToneId.Star)
                {
                    _speechSynthesizer.SpeakAsyncCancelAll();
                    InitializeState(_speechRecognitionEngine.RecognizerInfo.Culture.Name);
                    return;
                }

                switch (_workFlow.CurrentState)
                {
                    case CallWorkFlowState.InitialPrompt:
                        if (_tonesReceived < 1)
                        {
                            _speechSynthesizer.SpeakAsyncCancelAll();
                            _dtmfRecognitionEngine.AddTone((byte)e.Tone);
                            _tonesReceived++;
                        }
                        break;
                    case CallWorkFlowState.First2Digits:
                        if (_tonesReceived < 2)
                        {
                            _speechSynthesizer.SpeakAsyncCancelAll();
                            _dtmfRecognitionEngine.AddTone((byte)e.Tone);
                            _tonesReceived++;
                        }
                        break;
                    case CallWorkFlowState.AlphaDigits:
                        if (_tonesReceived < 2)
                        {
                            _speechSynthesizer.SpeakAsyncCancelAll();
                            _dtmfRecognitionEngine.AddTone((byte)e.Tone);
                            _tonesReceived++;
                        }
                        break;
                    case CallWorkFlowState.Last5Digits:
                        if (_tonesReceived < 5)
                        {
                            _speechSynthesizer.SpeakAsyncCancelAll();
                            _dtmfRecognitionEngine.AddTone((byte)e.Tone);
                            _tonesReceived++;
                        }
                        break;
                }
                
                UserInput userInput = new UserInput();
                userInput.IsDtmf = true;

                if (_workFlow.IsYesNoPrompt)
                {
                    _speechSynthesizer.SpeakAsyncCancelAll();

                    switch ((ToneId)e.Tone)
                    {
                        case ToneId.Pound:
                            userInput.Text = "yes";
                            _workFlow.HandleCallWorkFlow(userInput);
                            break;
                        case ToneId.Tone2:
                            userInput.Text = "no";
                            _workFlow.HandleCallWorkFlow(userInput);
                            break;
                        default:
                            _helper.TryAgain(true);
                            return;
                            break;
                    }
                }
                //else
                //{
                //    userInput.Text = e.Tone.ToString();

                //    //if (_workFlow.CurrentState == CallWorkFlowState.AlphaDigits)
                //    //{
                //    //    userInput.Text = "X";
                //    //}
                //    _workFlow.HandleToneWorkFlow(userInput);
                //}

                _idleTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
                _helper.DisconnectCall();
            }
        }

        #endregion

        #region Speech Recognition Events

        private void SpeechRecognitionEngine_SpeechDetected(object sender, SpeechDetectedEventArgs e)
        {
            _logger.Debug("Speech detected.");
        }

        private void SpeechRecognitionEngine_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            _logger.DebugFormat("Could not recognize");

            foreach (var item in e.Result.Words)
            {
                _logger.DebugFormat("Text: {0}", item.Text);
            }
        }

        private void SpeechRecognitionEngine_RecognizeCompleted(object sender, RecognizeCompletedEventArgs e)
        {
            try
            {
                _logger.Debug("recognize completed.");
                if (e.InitialSilenceTimeout)
                {
                    _silentAttempt++;
                    _logger.DebugFormat("Recognize InitialSilenceTimeout:{0}", e.InitialSilenceTimeout);
                    if (_silentAttempt > 3)
                    {
                        _helper.DisconnectingPrompt();
                        _helper.DisconnectCall();
                    }
                    else
                    {
                        _helper.TryAgain(true);
                    }
                }

                if (e.BabbleTimeout)
                {
                    _logger.DebugFormat("Recognize BabbleTimeout:{0}", e.BabbleTimeout);
                    _helper.DisconnectingPrompt();
                    _helper.DisconnectCall();
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
                _helper.DisconnectCall();
            }
        }

        private void speechRecognitionEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            try
            {
                _idleTimer.Stop();

                if (e.Result != null)
                {
                    if (currentState == CallWorkFlowState.LanguagePrompt)
                    {
                        _logger.DebugFormat("Text: {0}", e.Result.Text);

                        switch (e.Result.Text.ToLower())
                        {
                            case "one":
                                InitializeState(IvrConstant.CULTURE_NAME_ENGLIGH);
                                currentState = CallWorkFlowState.InitialPrompt;
                                break;
                            case "two":
                                InitializeState(IvrConstant.CULTURE_NAME_SPANISH);
                                currentState = CallWorkFlowState.InitialPrompt;
                                break;
                            default:
                                _helper.TryAgain(true);
                                break;
                        }
                        return;
                    }

                    _logger.DebugFormat("Current call work flow state:{0}", _workFlow.CurrentState);
                    _logger.DebugFormat("Text: {0}", e.Result.Semantics.Value.ToString());

                    UserInput userInput = new UserInput();
                    userInput.Text = e.Result.Semantics.Value.ToString();

                    _workFlow.HandleCallWorkFlow(userInput);
                }
                else
                {
                    //If neither grammar was used, this is not recognized speech.
                    //Console.WriteLine(string.Format("Could not recognize: {0} ", e.Result));
                    _logger.DebugFormat("Could not recognize: {0} ", e.Result);
                }

                // _waitForRecoCompleted.Set();
                _idleTimer.Start();
            }
            catch(Exception ex)
            {
                _logger.ErrorFormat("Exception : {0}", ex);
                _helper.DisconnectCall();
            }
        }

        #endregion

        #region Launching multiple threads
        public IncomingCallMain(AudioVideoCall avCall)
        {
            _avCall = avCall;
            _avCall.AudioVideoFlowConfigurationRequested += AudioVideoCall_FlowConfigurationRequested;
            _avCall.StateChanged += AudioVideoCall_StateChanged;

            waitToProcessCall = new AutoResetEvent[] {
                waitForAudioVideoFlowStateChangedToActiveCompleted, 
                waitForAudioVideoCallEstablished
            };

            ThreadPool.QueueUserWorkItem(ProcessCall);
        }

        public void AudioVideoCall_FlowConfigurationRequested(object sender, AudioVideoFlowConfigurationRequestedEventArgs e)
        {
            _avCallFlow = e.Flow;
            _avCallFlow.StateChanged += new EventHandler<MediaFlowStateChangedEventArgs>(AudioVideoFlow_StateChanged);
        }

        private void AudioVideoCall_StateChanged(object sender, Microsoft.Rtc.Collaboration.CallStateChangedEventArgs e)
        {
            _logger.InfoFormat("State changed from '{0}' to '{1}'", e.PreviousState, e.State);
            if (e.State == CallState.Established)
            {
                waitForAudioVideoCallEstablished.Set();
            }
        }

        private void AudioVideoFlow_StateChanged(object sender, MediaFlowStateChangedEventArgs e)
        {
            _logger.InfoFormat("Flow Handler Call state changed from '{0}' to '{1}'", e.PreviousState, e.State);
            if (e.State == MediaFlowState.Active)
            {
                waitForAudioVideoFlowStateChangedToActiveCompleted.Set();
            }
        }

        public void CloseResetEvents()
        {
            waitForAudioVideoFlowStateChangedToActiveCompleted.Close();
            waitForAudioVideoCallEstablished.Close();
        }

        #endregion
    }
}

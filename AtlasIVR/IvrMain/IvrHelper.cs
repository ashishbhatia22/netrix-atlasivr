﻿using log4net;
using Microsoft.Rtc.Collaboration;
using Microsoft.Rtc.Collaboration.AudioVideo;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.Synthesis;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Text;
using System.Xml.Serialization;

namespace CommunicationServiceWithConsole.IvrMain
{
    public class IvrHelper
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IvrHelper));
        private SpeechSynthesizer _speechSynthesizer;
        private SpeechRecognitionEngine _speechRecognitionEngine;
        private AudioVideoCall _avCall;
        private bool recognitionCompleted = false;
        private AutoResetEvent _waitForRecoCompleted;
        private DtmfRecognitionEngine _dtmfRecognitionEngine;
        private SpeechRecognitionConnector _speechRecognitionConnector;
        private SpeechSynthesisConnector _speechSynthesisConnector;
        private PromptData _promptData;
        private Dictionary<string, List<AlphaDigitMapping>> _alphaDigitMaster;
        private readonly static object syncRoot = new object();
        private IvrReport _ivrReport;

        public IvrHelper(SpeechSynthesizer synthesizer, SpeechRecognitionEngine recognitionEngine,             
            AudioVideoCall avCall, AutoResetEvent recoEvent,
            DtmfRecognitionEngine dtmfRecognitionEngine,
            SpeechRecognitionConnector speechRecognitionConnector,
            SpeechSynthesisConnector speechSynthesisConnector,
           Dictionary<string, List<AlphaDigitMapping>> alphaDigitMaster, IvrReport ivrReport)
        {
            _speechRecognitionEngine = recognitionEngine;
            _speechSynthesizer = synthesizer;
            _avCall = avCall;
            _waitForRecoCompleted = recoEvent;
            _dtmfRecognitionEngine = dtmfRecognitionEngine;
            _speechRecognitionConnector = speechRecognitionConnector;
            _speechSynthesisConnector = speechSynthesisConnector;
            _alphaDigitMaster = alphaDigitMaster;
            _ivrReport = ivrReport;

            lock (syncRoot)
            {
                LoadPromptData();
            }
        }

        private void LoadPromptData()
        {
            XmlSerializer xs = new XmlSerializer(typeof(PromptData));
            
            string filePath = System.Configuration.ConfigurationManager.AppSettings["IVRDeploymentFiles"];
            
            //@"C:\AtlasCodeShare";

            if (_speechRecognitionEngine.RecognizerInfo.Culture.Name == IvrConstant.CULTURE_NAME_SPANISH)
            {
                filePath = Path.Combine(filePath,"Spanish.xml");
            }
            else
            {
                filePath = Path.Combine(filePath,"English.xml");
            }

            var fileStream = new FileStream(filePath, FileMode.Open);

            _promptData = (PromptData)xs.Deserialize(fileStream);

            fileStream.Dispose();
        }

        public void AskLast5Digits(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.Last5Numbers, PromptAudioPath = "006.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void AskZipCode(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.AskZipCode, PromptAudioPath = "006.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void AskState(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = "Please say or enter a state.", PromptAudioPath = "state.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void ReferenceNumberNotFound(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.ReferenceNumberNotFound, PromptAudioPath = "NoReferenceNumberfound.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPrompt(promptData, recognize);
        }

        public void AgentNotFoundZipCode(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.AgentNotFound, PromptAudioPath = "NoReferenceNumberfound.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPrompt(promptData, recognize);
        }

        public void ReferenceNumberPending(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.ReferenceNumberPending, PromptAudioPath = "Pending.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPrompt(promptData, recognize);

            //WaitOne()
            // Disconnect
        }

        

        public void TransferCall(string name, string phoneNumber,bool recognize = true)
        {
            // If we are transferring
            //PromptBuilder builder = new PromptBuilder();
            //builder.AppendText("You will now be forwarded to your atlas representative ");
            //builder.AppendText(name);
            //_speechSynthesizer.Speak(builder);
            PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
            string transferPrompt = string.Format("{0} {1} at ", _promptData.FinalConfirmation, name);
            builder.AppendText(transferPrompt);

            foreach (char word in phoneNumber.ToCharArray())
            {
                builder.AppendBreak(new TimeSpan(5000));
                builder.AppendText(word.ToString());
            }
                       

            _speechSynthesizer.Speak(builder);

           
            }

        public void AskCity(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = "Please say or enter the destination city.", PromptAudioPath = "City.wav" };
            // Console.WriteLine("SynthNateame()");
            SpeakPromptAsync(promptData, recognize);
        }


        public void AskAlphaDigits(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.AlphaCharacters, PromptAudioPath = "005.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void PromptConfirmation(string resultCapture, bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = String.Format("{0}, {1} {2}.",_promptData.Confirmation, resultCapture, _promptData.PoundConfirmation), PromptAudioPath = "" };
            SpeakPromptAsync(promptData, recognize);
        }

        public void PromptAlphaDigitConfirmation(string resultCapture)
        {
            try
            {
                // look in dictionary and then create the prompt;
                List<AlphaDigitMapping> alphaDigitMapper = _alphaDigitMaster[resultCapture];

                PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                //builder.AppendText(string.Concat(_promptData.Press1Confirmation, alphaDigitMapper[0].Alpha));
                builder.AppendText(string.Format("{0} '{1}'", _promptData.Press1Confirmation, alphaDigitMapper[0].Alpha));
                builder.AppendBreak(new TimeSpan(5000));
                //builder.AppendText(string.Concat(_promptData.Press2Confirmation, alphaDigitMapper[1].Alpha));
                builder.AppendText(string.Format("{0} '{1}'", _promptData.Press2Confirmation, alphaDigitMapper[1].Alpha));
                builder.AppendBreak(new TimeSpan(5000));
                //builder.AppendText(string.Concat(_promptData.Press3Confirmation, alphaDigitMapper[2].Alpha));
                if (_speechRecognitionEngine.RecognizerInfo.Culture.ToString().Equals(IvrConstant.CULTURE_NAME_SPANISH) && resultCapture.Equals("8"))
                {
                    builder.AppendText(string.Format("{0} '{1}'", _promptData.Press3Confirmation, "ve"));
                }
                else
                {
                    builder.AppendText(string.Format("{0} '{1}'", _promptData.Press3Confirmation, alphaDigitMapper[2].Alpha));
                }

                if (resultCapture.Equals("7") || resultCapture.Equals("9"))
                {
                    builder.AppendBreak(new TimeSpan(5000));
                    //builder.AppendText(string.Format(_promptData.Press4Confirmation, alphaDigitMapper[3].Alpha));
                    builder.AppendText(string.Format("{0} '{1}'", _promptData.Press4Confirmation, alphaDigitMapper[3].Alpha));
                }

                _speechSynthesizer.SpeakAsync(builder);

                RecognizeName();
            }
            catch (Exception ex)
            {
                // TODO: log exception
                _logger.Error(ex);
            }
        }

        public void PromptAlphaDigitDTMF(int digitOrder)
        {
            string promptText = String.Empty;

            if (digitOrder.Equals(1))
            {
                promptText = _promptData.FirstAlphaDigit; // "Please enter the first digit of alpha reference number";
            }
            else
            {
                promptText = _promptData.SecondAlphaDigit; // "Please enter the second digit of alpha reference number";
            }
            var promptData = new PromptAudio() { PromptText = promptText, PromptAudioPath = "" };
            SpeakPromptAsync(promptData, true);
        }


        public void PromptConfirmationWithDelay(List<string> resultCapture, bool recognize = true)
        {
            try
            {
                PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                builder.AppendText(_promptData.Confirmation + ",");
                foreach (string word in resultCapture)
                {
                    builder.AppendBreak(new TimeSpan(5000));
                    builder.AppendText(word);
                }

                builder.AppendBreak(new TimeSpan(5000));
                builder.AppendText(_promptData.PoundConfirmation);

                _speechSynthesizer.SpeakAsync(builder);

                if (recognize)
                {
                    RecognizeName();
                }
            }
            catch (Exception ex)
            {
                // TODO: log exception
                _logger.Error(ex);
            }
        }

        public void AskFirst2Digits(bool recognize = true)
        {
            var promptData = new PromptAudio() { PromptText = _promptData.First2Digits, PromptAudioPath = "003.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPromptAsync(promptData, recognize);
        }

        public void AskUserToWait()
        {
            var promptData = new PromptAudio() { PromptText = _promptData.AskUserToWait, PromptAudioPath = "NoReferenceNumber.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPrompt(promptData, false);
            //Thread.Sleep(4000);
            DisconnectingPrompt();
            //Thread.Sleep(4000);
            // Disconnect call
            DisconnectCall();
        }


        public void AskUserToWait1()
        {
            var promptData = new PromptAudio() { PromptText = _promptData.AskUserToWait, PromptAudioPath = "NoReferenceNumber.wav" };
            // Console.WriteLine("SynthName()");
            SpeakPrompt(promptData, false);
            //Thread.Sleep(4000);
          
        }

        public void DisconnectingPrompt()
        {
            var promptData = new PromptAudio() { PromptText = _promptData.Disconnecting };
            // Console.WriteLine("SynthName()");
            SpeakPrompt(promptData, false);
        }

        public void TryAgain(bool recognize)
        {
            var promptData = new PromptAudio() { PromptText = string.Format("{0}{1}", _promptData.DidNotUnderstand, _promptData.TryAgain) };
            SpeakPromptAsync(promptData, recognize);
        }

        public void InvalidSelection(bool recognize)
        {
            var promptData = new PromptAudio() { PromptText = "Not a valid option. Please select a valid option." };
            SpeakPromptAsync(promptData, recognize);
        }

        public void LanguagePrompt()
        {
            //var promptData = new PromptAudio() { PromptText = "Hola..." };
            var promptData = new PromptAudio() { PromptText = "Thank you for calling Atlas, To continue in Spanish, Press 2." };
            PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
            builder.AppendText("Thank you for calling Atlas");
            _speechSynthesizer.SpeakAsync(builder);
            SpeechSynthesizer spanishSynthesizer = new SpeechSynthesizer();

            //SpeakPromptAsync(promptData, true);
        }

        public void SpanishNotReady(bool recognize)
        {
            var promptData = new PromptAudio() { PromptText = "We are working on Spanish... Please, press 1 for English." };
            SpeakPromptAsync(promptData, recognize);
        }

        public void InitialPrompt()
        {
            var promptData = new PromptAudio() { PromptText = _promptData.InitialPrompt };
            SpeakPromptAsync(promptData, true);
        }

        public void NotAbleToUnderstand()
        {
            var promptData = new PromptAudio() { PromptText = _promptData.NotAbleToUnderstand };
            SpeakPrompt(promptData, true);
          //  var promptData = new PromptAudio() { PromptText = "I am having difficulty understanding you. Please use touch key-pad" };
           // helper.SpeakPrompt(promptData);
        }

        public void SpeakPromptAsync(PromptAudio input, bool recognize = true)
        {
            try
            {
                _logger.Debug("ThreadId: " + Thread.CurrentThread.ManagedThreadId);
                // temporarily set the audio path to null to have a consistent user experience
                input.PromptAudioPath = null;

                if (string.IsNullOrEmpty(input.PromptAudioPath))
                {
                    PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                    builder.AppendText(input.PromptText);
                    _speechSynthesizer.SpeakAsync(builder);
                   // _speechSynthesizer.SpeakAsync(input.PromptText);
                }
                else
                {
                    const string path = @"C:\atlasIVR - Copy\AtlasIVR\Prompts";

                    PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                    builder.AppendAudio(Path.Combine(path, input.PromptAudioPath));
                    builder.AppendBreak(new TimeSpan(500));
                    // builder.AppendAudio(@"C:\atlasIVR - Copy\AtlasIVR\Prompts\002.wav");
                    _speechSynthesizer.SpeakAsync(builder);
                }

                if (recognize)
                {
                    RecognizeName();
                }
            }
            //catch (System.IO.EndOfStreamException)
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
                //Eat this exception..
            }

        }

        public void SpeakPrompt(PromptAudio input, bool recognize = true)
        {
            try
            {
                _logger.Debug("ThreadId: " + Thread.CurrentThread.ManagedThreadId);
                // temporarily set the audio path to null to have a consistent user experience
                input.PromptAudioPath = null;
                if (string.IsNullOrEmpty(input.PromptAudioPath))
                {
                    PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                    builder.AppendText(input.PromptText);
                   /// _speechSynthesizer.SpeakAsync(builder);
                    _speechSynthesizer.Speak(builder);
                   // _speechSynthesizer.Speak(input.PromptText);
                }
                else
                {
                    const string path = @"C:\atlasIVR - Copy\AtlasIVR\Prompts";

                    PromptBuilder builder = new PromptBuilder(_speechRecognitionEngine.RecognizerInfo.Culture);
                    
                    builder.AppendAudio(Path.Combine(path, input.PromptAudioPath));
                    builder.AppendBreak(new TimeSpan(500));
                    
                    // builder.AppendAudio(@"C:\atlasIVR - Copy\AtlasIVR\Prompts\002.wav");
                    _speechSynthesizer.SpeakAsync(builder);
                   // _speechSynthesizer.Speak(builder);
                }

                if (recognize)
                {
                    RecognizeName();
                }
            }
            //catch (System.IO.EndOfStreamException ex)
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
                //Eat this exception..
            }
        }

        public void RecognizeName()
        {
            Console.WriteLine("\r\nRecognizing...");
            _logger.Debug("Recognizing...");
            try
            {
                //_speechRecognitionEngine.InitialSilenceTimeout = new TimeSpan(0, 0, 5);
                //_speechRecognitionEngine.BabbleTimeout = new TimeSpan(0, 0, 30);
                //_speechRecognitionEngine.EndSilenceTimeout = new TimeSpan(0, 0, 5);

                _speechRecognitionEngine.RecognizeAsync(RecognizeMode.Multiple);
                
                //_dtmfRecognitionEngine.RecognizeAsync();
               // _waitForRecoCompleted.WaitOne();
            }
            catch (Exception ex)
            {
                _logger.Error("Exception while recognizing...");
                //Console.WriteLine(ex.Message);
            }
        }

        public void TransferCall(string transferToNumber)
        {
            CallTransferOptions basicTransfer = new CallTransferOptions(CallTransferType.Unattended);

            _avCall.BeginTransfer(transferToNumber, basicTransfer, EndTransfer, null);

        }

        private void EndTransfer(IAsyncResult ar)
        {
            try
            {
                _avCall.EndTransfer(ar);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            //  throw new NotImplementedException();
        }

        public void DisconnectCall()
        {
            try
            {
                _ivrReport.CallEndTime = DateTime.Now;
                IvrDA ivrDA = new IvrDA();
                ivrDA.SaveReportData(_ivrReport);
               // _ivrReport
               // _ivrReport.WorkFlowStatus = Curr
                // Delay for 2 seconds before dis-connecting
                Thread.Sleep(1000);
                //DisconnectingPrompt();
                _avCall.BeginTerminate(EndTerminateCall, _avCall);
            }
            catch(Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
            }
        }

        private void PersistReportData()
        {
            //TODO
        }



       



        private void EndTerminateCall(IAsyncResult ar)
        {
            try
            {
                AudioVideoCall AVCall = ar.AsyncState as AudioVideoCall;

                // Complete the termination of the incoming call.
                AVCall.EndTerminate(ar);

                Console.WriteLine("Terminated the call...");
                _logger.Debug("Terminated the call...");

                //_speechRecognitionConnector.Stop();
                //_speechRecognitionConnector.DetachFlow();

                //_speechSynthesisConnector.Stop();
                //_speechSynthesisConnector.DetachFlow();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception: {0}", ex);
            }
        }
        
        public void LoadGrammar(GrammerRule ruleName)
        {
            string fileName = string.Empty;
            _speechRecognitionEngine.UnloadAllGrammars();
            _logger.DebugFormat("Unloaded all grammers.");

            var currentPath = System.Configuration.ConfigurationManager.AppSettings["IVRDeploymentFiles"];
            //@"C:\atlasIVR - Copy\Atlas IVR\AtlasIVR\AtlasIVR\bin\Debug";
            _logger.DebugFormat("Current Executable Path.{0}", currentPath);

            switch (ruleName)
            {
                case GrammerRule.onedigit_voice:
                    fileName = "OneDigitVoice.grxml";
                    break;
                case GrammerRule.twodigits_voice:
                    fileName = "TwoDigitsVoice.grxml";
                    break;
                case GrammerRule.alphadigits_voice:
                    fileName = "AlphaDigitsVoice.grxml";
                    break;
                case GrammerRule.fivedigits_voice:
                    fileName = "FiveDigitsVoice.grxml";
                    break;
                case GrammerRule.state_voice:
                    fileName = "StateVoice.grxml";
                    break;
                case GrammerRule.city_voice:
                    fileName = "CityVoice.grxml";
                    break;
                case GrammerRule.Confirmation_Voice:
                    fileName = "ConfirmationVoice.grxml";
                    break;
                case GrammerRule.LanguagePrompt:
                    fileName = "LanguagePrompt.grxml";
                    break;
                default:
                    break;

            }

            Grammar voiceGrammar;
            Grammar mainMenuVoiceGrammar;
            if (_speechRecognitionEngine.RecognizerInfo.Culture.Name == IvrConstant.CULTURE_NAME_SPANISH)
            {
               
                voiceGrammar = new Grammar(Path.Combine(currentPath,@"Grammars\Spanish\") + fileName, ruleName.ToString());
                mainMenuVoiceGrammar = new Grammar(Path.Combine(currentPath,@"Grammars\Spanish\MainMenuVoice.grxml"), "MainMenu_Voice");
            }
            else
            {
                voiceGrammar = new Grammar(Path.Combine(currentPath,@"Grammars\") + fileName, ruleName.ToString());
                mainMenuVoiceGrammar = new Grammar(Path.Combine(currentPath,@"Grammars\MainMenuVoice.grxml"), "MainMenu_Voice");
            }

            voiceGrammar.Name = ruleName.ToString();

            _speechRecognitionEngine.LoadGrammar(mainMenuVoiceGrammar);
            _speechRecognitionEngine.LoadGrammar(voiceGrammar);
            //_dtmfRecognitionEngine.LoadGrammarAsync(twoDigitGrammer);

            _logger.DebugFormat("Loaded {0} grammer.", ruleName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunicationServiceWithConsole.IvrMain
{
    public enum CallWorkFlowState
    {
        LanguagePrompt,
        InitialPrompt,
        First2Digits,
        AlphaDigits,
        Last5Digits,
        State,
        City,
        AlphaDigit1,
        AlphaDigit2,
        ZipCode
    }

    public enum GrammerRule
    {
        onedigit_voice,
        twodigits_voice,
        alphadigits_voice,
        fivedigits_voice,
        city_voice,
        state_voice,
        Confirmation_Voice,
        onedigit_dtmf,
        twodigits_dtmf,
        alphadigits_dtmf,
        fivedigits_dtmf,
        LanguagePrompt
    }
}

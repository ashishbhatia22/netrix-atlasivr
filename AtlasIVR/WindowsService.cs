﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Configuration;
using System.Threading;
using Microsoft.Rtc.Collaboration;
using Microsoft.Rtc.Collaboration.AudioVideo;
using Microsoft.Rtc.Collaboration.AudioVideo.VoiceXml;

using Microsoft.Rtc.Signaling;
using Microsoft.Speech.VoiceXml.Common;
using CommunicationServiceWithConsole.IvrMain;
using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;

namespace CommunicationServiceWithConsole
{
    partial class WindowsService : ServiceBase
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(WindowsService));
        private static Browser voiceXmlBrowser;
        private static Uri startPage; //A global keeper for the URI to the VXML start page.

        private static TraceSwitch tracer = new TraceSwitch("AtlasIVR", "Atlas IVR App");
        // Create the necessary Call variables
        private CollaborationPlatform _collabPlatform;
        private ApplicationEndpoint _endpoint;
        private AudioVideoCall _avCall;
        //private static AudioVideoFlow _audioVideoFlow;
        private static object callCleanupLock = new object();
        private static string[] _states;
        private static List<string> _cities = new List<string>();
        //// A wait handle for startup and one for shutdown.
        //// They are set to unsignaled to start.
        ManualResetEvent _startupWaitHandle = new ManualResetEvent(false);
        //
        ManualResetEvent _shutdownWaitHandle = new ManualResetEvent(false);

        AutoResetEvent _waitForCallToBeAccepted = new AutoResetEvent(false);
        
        public WindowsService()
        {
            _logger.Info("Preparing to Initialize UCMA app");
            //Trace.WriteLineIf(tracer.TraceVerbose, "Preparing to Initialize UCMA app");
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Initialize();
        }

        public void WaitForStartup()
        {
            _startupWaitHandle.WaitOne();

        }

        protected override void OnStop()
        {
            Cleanup();
        }
        
        private void Initialize()
        {
            //Trace.WriteLineIf(tracer.TraceVerbose, "Initializing the UCMA Runtime");
            //writeLog("Initializing the UCMA Runtime");
            _logger.Info("Initializing the UCMA Runtime");

            string applicationId = System.Configuration.ConfigurationManager.AppSettings["ApplicationId"];

            ProvisionedApplicationPlatformSettings platformSettings = new ProvisionedApplicationPlatformSettings("Atlas IVR App", applicationId);


          //  ProvisionedApplicationPlatformSettings platformSettings = new ProvisionedApplicationPlatformSettings("Atlas IVR App", "urn:application:IVRprod");
            // Populate other properties on platformSettings.
            _collabPlatform = new CollaborationPlatform(platformSettings);

            // Register a handler to be alerted of each endpoint's configuration as it is discovered.
            _collabPlatform.RegisterForApplicationEndpointSettings(ApplicationEndpointDiscoveredHandler);

            // Register a handler to be alerted of non-fatal failures after initial startup has succeeded.
            _collabPlatform.ProvisioningFailed += ProvisioningFailedHandler;
            _collabPlatform.BeginStartup(PlatformStartupCallback, null);

            //Trace.WriteLineIf(tracer.TraceInfo, "Initialize Complete");
            //writeLog("Initialize complete");
            _logger.Info("Initialize complete");
        }
        
        private static void Cleanup()
        {
            //writeLog("Starting Cleanup...");
            //writeLog("Finished Cleanup");
            _logger.Info("Finished Cleanup");
        }

        // Event Handlers
        private void ApplicationEndpointDiscoveredHandler(object sender, ApplicationEndpointSettingsDiscoveredEventArgs e)
        {
            //Trace.WriteLineIf(tracer.TraceVerbose, "ApplicationEndpointDiscoveredHandler");
            //writeLog("ApplicationEndpointDiscoveredHandler");
            _logger.Info("ApplicationEndpointDiscoveredHandler");

            ApplicationEndpointSettings endpointSettings = e.ApplicationEndpointSettings;

            _endpoint = new ApplicationEndpoint(_collabPlatform, endpointSettings);

            //Trace.WriteLineIf(tracer.TraceVerbose, "Registering for incomming calls...");
            //writeLog("Registering for incomming calls...");
            _logger.Info("Registering for incomming calls...");

            _endpoint.RegisterForIncomingCall<AudioVideoCall>(AudioVideoCallReceived);
            //_endpoint.RegisterForIncomingCall<AudioVideoCall>(AVCall_Received);
            
            //Trace.WriteLineIf(tracer.TraceVerbose, "Establishing Endpoint...");
            //writeLog("Establishing Endpoint...");
            _logger.Info("Establishing Endpoint...");

            _endpoint.EndEstablish(_endpoint.BeginEstablish(null, _endpoint));

            //Trace.WriteLineIf(tracer.TraceInfo, "ApplicationEndpointDiscoveredHandler Complete...");
            //writeLog("ApplicationEndpointDiscvoveredHandler Complete...");
            _logger.Info("ApplicationEndpointDiscvoveredHandler Complete...");
            Console.WriteLine("Ready to accept calls.");
        }
        private void ProvisioningFailedHandler(object sender, ProvisioningFailedEventArgs e)
        {
            //writeLog("ProvisioningFailedHandler");
            //Trace.Fail("ProvisioningFailedHandler");
            _logger.Info("ProvisioningFailedHandler");
        }
        private void PlatformStartupCallback(IAsyncResult result)
        {
            try
            {
                //Trace.WriteLineIf(tracer.TraceVerbose, "PlatformStartupCallback...");
                //writeLog("PlatformStartupCallback...");
                _logger.Info("PlatformStartupCallback");
                _collabPlatform.EndStartup(result);

                _collabPlatform.ConnectionManager.StrictDispatching = false;
                //Trace.WriteLineIf(tracer.TraceVerbose, "PlatformStartupCallback Done");
                //writeLog("PlatformStartupCallback Done");
                _logger.Info("PlatformStartupCallback Done");
            }
            catch (RealTimeException ex)
            {
                //writeLog(string.Format("Platform startup failed: {0}", ex));
                _logger.Error(string.Format("Platform startup failed: {0}", ex));
            }

        }

        private static object syncRoot = new object();

        private void AVCall_Received(object sender, CallReceivedEventArgs<AudioVideoCall> e)
        {
            _logger.Info(string.Format("AudioVideoCallReceived: {0}", DateTime.Now));
            _avCall = e.Call;

            if (e.IsNewConversation)
            {
                lock (syncRoot)
                {
                    //var incomingCall = new IncomingCallMain(_avCall);

                    try
                    {
                        _avCall.BeginAccept((asyncResult) =>
                        {
                            try
                            {
                                _avCall.EndAccept(asyncResult);
                            }
                            catch (RealTimeException rte)
                            {
                                _logger.ErrorFormat("Error accepting incoming AV call {0}", rte);
                            }
                        },
                        null);
                    }
                    catch (InvalidOperationException ioe)
                    {
                        _logger.ErrorFormat("Error accepting incoming AV call {0}", ioe);
                    }
                }
            }
        }
        private void AudioVideoCallReceived(object sender, CallReceivedEventArgs<AudioVideoCall> e)
        {
            _logger.InfoFormat("CallID: {0}", e.Call.CallId);

            lock (syncRoot)
            {
                _logger.Info(string.Format("Entered inside lock at: {0}", DateTime.Now));
                //Thread.Sleep(10000);
                try
                {
                    _logger.Info("Begin Accepting incoming call");
                    _avCall = e.Call;
                    
                    e.Call.BeginAccept((asyncResult) =>
                    {
                        try
                        {
                            e.Call.EndAccept(asyncResult);

                            _logger.Info("Accepted incoming call");

                            //e.Call.Flow.StateChanged += new EventHandler<MediaFlowStateChangedEventArgs>(OnFlowStateChanged);

                            ProcessCall(e.Call);
                            //_logger.DebugFormat("Current state: {0}", e.Call.Flow.State);

                            //if (e.Call.Flow.State.Equals(MediaFlowState.Active))
                            //{
                            //    IncomingCallMain t = new IncomingCallMain();
                            //    t.PromptUserForInput(e.Call);

                            //}
                        }
                        catch (RealTimeException rte)
                        {
                            _logger.ErrorFormat("Error accepting incoming AV call {0}", rte);
                        }
                    },
                    null);
                }
                catch (InvalidOperationException ioe)
                {
                    _logger.ErrorFormat("Error accepting incoming AV call {0}", ioe);
                }

                Thread.Sleep(200);
            }
        }

        private void ProcessCall(AudioVideoCall avCall)
        {
            switch (avCall.Flow.State)
            {
                case MediaFlowState.Active:
                    _logger.Debug("Current state: Active");
                    //IncomingCallMain t = new IncomingCallMain();
                    //t.PromptUserForInput(avCall);
                    ThreadPool.QueueUserWorkItem(ProcessNewCall, avCall);
                    break;
                case MediaFlowState.Idle:
                    _logger.Debug("Current state: Idle");
                    Thread.Sleep(100);
                    ProcessCall(avCall);
                    break;
                case MediaFlowState.Terminated:
                    _logger.Debug("Current state: Terminated");
                    break;
            }
        }

        private void ProcessNewCall(object state)
        {
            AudioVideoCall avCall = state as AudioVideoCall;
            IncomingCallMain t = new IncomingCallMain();
            t.PromptUserForInput(avCall);
        }

        /// <summary>
        /// EventHandler raised when an incoming invite arrives.
        /// This is the kick-off point for the call flow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private Object thisLock = new Object();

        #region Old
        //private void AudioVideoCallReceived(object sender, CallReceivedEventArgs<AudioVideoCall> e)
        //{
        //    lock (this)
        //    {
        //        //Trace.WriteLineIf(tracer.TraceVerbose, "AudioVideoCallReceived");
        //        //writeLog(string.Format("AudioVideoCallReceived: {0}", DateTime.Now));
        //        _logger.Info(string.Format("AudioVideoCallReceived: {0}", DateTime.Now));
        //        Debug.Assert(e != null, "e != null");
        //        //Debug.Assert(e.RequestData != null, "e.RequestData != null");
        //        _avCall = e.Call;

        //        //Accept the Call
        //        try
        //        {
        //            //Trace.WriteLineIf(tracer.TraceVerbose,"Begin Accepting incoming call");
        //            //writeLog(string.Format("Begin Accepting incoming call"));
        //            _logger.Info("Begin Accepting incoming call");
        //            _waitForCallToBeAccepted.WaitOne(1000);

        //            e.Call.BeginAccept(OnCallAcceptCompleted, null);

        //        }
        //        catch (RealTimeException ex)
        //        {
        //            //Trace.WriteLineIf(tracer.TraceVerbose, "Failed accepting incomming call");
        //            //writeLog(string.Format("Failed excepting incoming call: {0}", ex));
        //            _logger.ErrorFormat("Failed excepting incoming call: {0}", ex);
        //        }
        //    }
        //}
        //private void OnCallAcceptCompleted(IAsyncResult result)
        //{
        //    try
        //    {
        //        _avCall.EndAccept(result);
        //        _logger.InfoFormat("Accepted incoming call: {0}", DateTime.Now);
        //        //Trace.WriteLineIf(tracer.TraceVerbose, "Accepted incoming call");
        //        //writeLog(string.Format("Accepted incoming call: {0}", DateTime.Now));
        //        //Attach an event handler to to detect the flow termination
        //        //_audioVideoFlow = _avCall.Flow;
        //        _avCall.Flow.StateChanged += new EventHandler<MediaFlowStateChangedEventArgs>(OnFlowStateChanged);

        //        _logger.DebugFormat("Current state: {0}", _avCall.Flow.State);

        //        //if (_avCall.Flow.State == MediaFlowState.Active)
        //        //{
        //        IncomingCallMain t = new IncomingCallMain();
                    
        //            t.PromptUserForInput(_avCall);
        //            //t.RecognizeCall(_avCall);
                   
        //            //RunBrowser();
        //        //}
                
        //        _logger.DebugFormat("audioVideFlow attached");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.ErrorFormat("Failed excepting incoming call: {0}", ex);
        //    }
        //}
        #endregion
        private void OnFlowStateChanged(object sender, MediaFlowStateChangedEventArgs e)
        {
            //_waitForCallToBeAccepted.Set();
            _logger.InfoFormat("State changed from '{0}' to '{1}'", e.PreviousState, e.State);

            //if (e.State == MediaFlowState.Terminated)
            //{
            //    CleanupCall();
            //}
        }
        
        /// <summary>
        /// Initializes the Browser object and sets event handlers
        /// </summary>
        private void InitializeVoiceXmlBrowser()
        {
            // dispose any existing Browser reference before we instantiate a new one
            if (voiceXmlBrowser == null)
            {
                //Create the browser object, and bind all associated event handlers. 
                voiceXmlBrowser = new Browser();

                //These events are analogues to the similarly named call states.
                voiceXmlBrowser.Transferring += new EventHandler<TransferringEventArgs>(HandleTransferring);
                voiceXmlBrowser.Transferred += new EventHandler<TransferredEventArgs>(HandleTransferred);
               
                voiceXmlBrowser.Disconnecting
                    += new EventHandler<DisconnectingEventArgs>(HandleDisconnecting);
                voiceXmlBrowser.Disconnected
                    += new EventHandler<DisconnectedEventArgs>(HandleDisconnected);
              
                voiceXmlBrowser.SessionCompleted
                    += new EventHandler<SessionCompletedEventArgs>(HandleSessionCompleted);
                
            }
        }

        private void Flow_StateChanged(object sender, MediaFlowStateChangedEventArgs e)
        {
            if (e.State == MediaFlowState.Active)
            {
                RunBrowser();
            }
        }

        /// <summary>
        /// Run the browser
        /// </summary>
        private void RunBrowser()
        {
            InitializeVoiceXmlBrowser();

            //If the start page has not been assigned, fail with appropriate message box.
            //More robust error checking is left as an exercise to the user.
            SetStartPage();

            var container = new System.Net.CookieContainer();

            //container.SetCookies(uri, "name=value1");
            container.SetCookies(startPage, "Name=Marshall");

            //Associate the avCall with the VXML browser object. 
            voiceXmlBrowser.SetAudioVideoCall(_avCall);

            //Calling RunAsync on the browser object will cause the VXML browser page to execute on the associated AVCall.
            //Note, the Async model used here is not the Begin/End type prevalent elsewhere in UCMA. 
            //Trace.WriteLineIf(tracer.TraceVerbose, "vxmlBrowser: Starting");
            //writeLog("vxmlBrowser: Starting");
            voiceXmlBrowser.RunAsync(startPage, container);
        }

        private void SetStartPage()
        {
            startPage = null;
            string vxmlFilename = string.Empty;
            vxmlFilename = ConfigurationManager.AppSettings["startupPage"];
            //vxmlFilename = "vxml\\IVRPage1.vxml";
            startPage = new Uri(new Uri(System.Environment.CurrentDirectory + "\\"), vxmlFilename);

        }
        private void HandleDisconnecting(object sender, DisconnectingEventArgs e)
        {
            //Trace.WriteLineIf(tracer.TraceVerbose, "vxmlBrowser: Disconnecting");
            //writeLog("vxmlBrowser: Disconnecting");
        }
        private void HandleDisconnected(object sender, DisconnectedEventArgs e)
        {
            //Trace.WriteLineIf(tracer.TraceVerbose, "vxmlBrowser: Diconnected");
            //writeLog("vxmlBrowser: Diconnected");
        }

        private void HandleTransferring(object sender, TransferringEventArgs e)
        {
            //Trace.WriteLineIf(tracer.TraceVerbose, "vxmlBrowser: Transferring");
            //writeLog("vxmlBrowser: Transferring");
        }

        private void HandleTransferred(object sender, TransferredEventArgs e)
        {
            //Trace.WriteLineIf(tracer.TraceVerbose, "vxmlBrowser: Transferred");
            //writeLog("vxmlBrowser: Transferred");
        }

        private void HandleSessionCompleted(object sender, SessionCompletedEventArgs e)
        {
            //var results = new System.Net.CookieContainer();
            var results = e.Result.Cookies.GetCookies(startPage);

            //Trace.WriteLineIf(tracer.TraceVerbose, "vxmlBrowser: SessionCompleted");
            //writeLog("vxmlBrowser: SessionCompleted");
            //writeLog(e.Result.Reason.ToString());
            //It is good practice to check the call state after the session terminicates
            CleanupCall();

        }
        private void CleanupCall()
        {
            _logger.Debug("Cleanup the call.");
            lock (callCleanupLock)
            {
                if (_avCall != null)
                {
                    if (_avCall.Flow != null)
                    {   // remove the existing call handler
                        _avCall.Flow.StateChanged -= new EventHandler<MediaFlowStateChangedEventArgs>(OnFlowStateChanged);
                    }
                    // We have two options here:
                    // 1. the vxml call flow has finished but the call is still in an Established state
                    // 2. the user hung up in which case the call is in a Terminating state
                    // either way we need to clean up our end
                    // Note: need to hanldle calls that transfer
                    if (_avCall.State == CallState.Established || _avCall.State == CallState.Terminating)
                    {
                        _avCall.EndTerminate(_avCall.BeginTerminate(null, null));
                    }

                    _avCall = null;
                }
            }
        }
    }
}

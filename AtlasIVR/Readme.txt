﻿.NET includes a simple utility to install Services called installutil.exe.

To Install:
C:\Windows\Microsoft.NET\Framework\v2.0.50727\installutil "CommunicationServiceWithConsole.exe"

To Uninstall:
C:\Windows\Microsoft.NET\Framework\v2.0.50727\installutil /u "CommunicationServiceWithConsole.exe"
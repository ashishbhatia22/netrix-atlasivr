﻿using System;
using System.Diagnostics;
using System.Workflow.Activities;
using Microsoft.Speech.Recognition;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;
using System.DirectoryServices;


namespace CommunicationServiceWithConsole
{
    /// <summary>
    /// Represents the control flow for the application
    /// </summary>
    public partial class Workflow1 : SequentialWorkflowActivity
    {

        private string remoteParticipant;
        private string toHeader;
        private string transferredBy;
        private string resource;

        private string currentGreeting = string.Empty;
        private string newGreeting = string.Empty;
        private string customGreetingPath = string.Empty;

        public string RemoteParticipant
        {
            get { return remoteParticipant; }
            set { remoteParticipant = value; }
        }

        public string ToHeader
        {
            get { return toHeader; }
            set { toHeader = value; }
        }

        public string TransferredBy
        {
            get { return transferredBy; }
            set { transferredBy = value; }
        }

        public string Resource
        {
            get { return resource; }
            set { resource = value; }
        }

        private bool emailIsUrgent = false;
        private bool sentEmail = false;
        private bool missedCall = false;
        private string basePath = string.Empty;

        /// <summary>
        /// This method is called when any exception occurs within the workflow.  It does some 
        /// generic exception logging, and is provided for convenience during debugging; you 
        /// should replace or augment this with your own error handling code.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleGeneralFault(object sender, EventArgs e)
        {
            // When an exception is thrown the actual exception is stored in the Fault property,
            // which is read-only.  Check this value for error information; if it is an exception, 
            // ToString() will include a full stack trace of all inner exceptions.
            string errorMessage = generalFaultHandler.Fault.ToString();
            writeLog("HandleGeneralFault fired.");
            Trace.Write(errorMessage);
            writeLog(errorMessage);

            if (Debugger.IsAttached)
            {
                // If the debugger is attached, break here so that you can see the error that occurred.
                // (Check the errorMessage variable above.)
                Debugger.Break();
            }
        }

        private void codeSetBasePath_ExecuteCode(object sender, EventArgs e)
        {
            basePath = AppDomain.CurrentDomain.BaseDirectory;

        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// This method is called when a call is Disconnected while the workflow is still executing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleCallDisconnectedEvent(object sender, EventArgs e)
        {
            // Add your logic here.
            writeLog("Call Disconnect Handler fired.");
            if (resource == "VoiceMail")
                if (!sentEmail)
                    codeCreateEmail_ExecuteCode(sender, e);
        }

        #region RecordGreeting

        private void codeSetRecordPath_ExecuteCode(object sender, EventArgs e)
        {
            getCustomGreetingPath();
            // Set the file name for the new recording
            newGreeting = string.Format("{0}\\NewGreeting.wma", customGreetingPath);
            recordActivity1.FileName = newGreeting;
            writeLog(recordActivity1.FileName);

        }

        private void sayCurrentGreeting_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            sayCurrentGreeting.MainPrompt.ClearContent();
            sayCurrentGreeting.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\currgreeting.wav"), "Your current greeting is");
            sayCurrentGreeting.MainPrompt.AppendBreak();
            sayCurrentGreeting.MainPrompt.AppendAudio(currentGreeting);

        }

        private void askChangeIt_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            askChangeIt.MainPrompt.ClearContent();
            askChangeIt.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\changeit.wav"), "Press 1 to change your greeting, or presss 2 to cancel");

            this.askChangeIt.Grammars.Clear();
            // No ASR at this time per Kell
            // this.askChangeIt.Grammars.Add(new Grammar(basePath + @"\\Grammars\Library.grxml", "Confirmation_YesNo"));
            this.askChangeIt.DtmfGrammars.Add(new Grammar(
                    basePath + @"\\Grammars\PinDtmf.grxml", "digitsplus"));
        }

        private void sayAtTheTone_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            sayAtTheTone.MainPrompt.ClearContent();
            sayAtTheTone.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\atthetone.wav"), "Record your greeting after the tone, press pound when complete");
        }

        private void sayBeep_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            sayBeep.MainPrompt.ClearContent();
            sayBeep.MainPrompt.AppendAudio(basePath + "\\Prompts\\beep3.wav");
            sayBeep.MainPrompt.AppendAudio(basePath + "\\Prompts\\beep3.wav");
            sayBeep.MainPrompt.AppendAudio(basePath + "\\Prompts\\beep3.wav");

        }


        private void sayNewGreeting_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            sayNewGreeting.MainPrompt.ClearContent();
            sayNewGreeting.MainPrompt.AppendAudio(newGreeting);
        }

        private void askSaveIt_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            askSaveIt.MainPrompt.ClearContent();
            askSaveIt.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\saveit.wav"), "Press 1 to save your greeting, or Press 2 to cancel ");

            this.askSaveIt.Grammars.Clear();
            // No ASR at this time per Kelli
            // this.askSaveIt.Grammars.Add(new Grammar(basePath + @"\\Grammars\Library.grxml", "Confirmation_YesNo"));
            this.askSaveIt.DtmfGrammars.Add(new Grammar(
                    basePath + @"\\Grammars\PinDtmf.grxml", "digitsplus"));

        }

        private void saySaveGreeting_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            saySaveGreeting.MainPrompt.ClearContent();
            saySaveGreeting.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\greetingsav.wav"), "Your greeting has been saved.");
        }

        private void sayGoodbye_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            sayGoodbye.MainPrompt.ClearContent();
            sayGoodbye.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\goodbye.wav"), "Goodbye, and have a great day. ");
        }

        private void ifCaseChangeIt(object sender, ConditionalEventArgs e)
        {
            if (askChangeIt.RecognitionResult.Semantics.Value.ToString().ToUpper() == "1")
                e.Result = true;
            else
                e.Result = false;
        }


        private void ifCaseSaveIt(object sender, ConditionalEventArgs e)
        {

            if (askSaveIt.RecognitionResult.Semantics.Value.ToString().ToUpper() == "1")
                e.Result = true;
            else
                e.Result = false;
        }

        private void getCustomGreetingPath()
        {
            int startPos;

            // stip out the sip address for the caller.
            startPos = (remoteParticipant.IndexOf(":"));

            customGreetingPath = string.Format("C:\\Greetings\\{0}", remoteParticipant.Substring(startPos + 1));
            //writeLog(string.Format("******** CustomGreetingPath:{0}", customGreetingPath));

            // Now letsmake sure the file exists
            // If it doesn't then set the default greeting
            // Example: "c:\\Greetings\\klaw@uberuc.com\CustomGreeting.wav"
            currentGreeting = string.Format("{0}\\CustomGreeting.wav", customGreetingPath);
            if (!(File.Exists(currentGreeting)))
                currentGreeting = basePath + "\\Prompts\\default.wav";


        }



        private void writeLog(string logText)
        {
            string longFileName;
            try
            {
                Console.WriteLine(logText);

                longFileName = AppDomain.CurrentDomain.BaseDirectory + String.Format("\\RecordGreeting.txt");

                StreamWriter sw;
                if (File.Exists(longFileName))
                {
                    sw = File.AppendText(longFileName);
                }
                else
                {
                    sw = File.CreateText(longFileName);
                }

                sw.WriteLine(string.Format(@"{0}: {1}", DateTime.Now.ToString(), logText));
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }

        private void codeSaveGreeting_ExecuteCode(object sender, EventArgs e)
        {
            string custGreeting = string.Empty;

            try
            {
                custGreeting = string.Format("{0}\\customgreeting.wav", customGreetingPath);
                //writeLog(string.Format("Copying {0} to {1}", newGreeting, custGreeting));
                File.Copy(newGreeting, custGreeting, true);
            }
            catch (Exception ex)
            {
                writeLog(string.Format("Unable to save greeting: {0}", ex.Message));
            }
        }

        private void codeDeleteRecording_ExecuteCode(object sender, EventArgs e)
        {
            try
            {

                if (File.Exists(newGreeting))
                {
                    File.Delete(newGreeting);
                }
            }
            catch (Exception ex)
            {
                writeLog(string.Format("Unable to delete greeting: {0}", ex.Message));
            }

        }

        private void codeSetupDirectoryStructure_ExecuteCode(object sender, EventArgs e)
        {
            try
            {
                if (!(Directory.Exists(customGreetingPath)))
                {
                    Directory.CreateDirectory(customGreetingPath);
                }

            }
            catch (Exception ex)
            {
                writeLog(string.Format("Unable to create directory structure: {0}", ex.Message));
            }
        }

        private void ifCaseGreeting(object sender, ConditionalEventArgs e)
        {
            if (resource == "Greeting")
                e.Result = true;
            else
                e.Result = false;
        }
        #endregion

        #region StoreRecording
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region SR_Speech
        private void saySpeechGreeting_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            string promptPath = string.Empty;

            //writeLog("saySpeechGreeting_TurnStarting");
            saySpeechGreeting.MainPrompt.ClearContent();
            promptPath = getCustomGreeting();
            if (promptPath.Length > 0)
            {
                saySpeechGreeting.MainPrompt.AppendAudio(promptPath);
            }
            else
            {
                saySpeechGreeting.MainPrompt.AppendAudio(basePath + "\\Prompts\\default.wav");
                //saySpeechGreeting.MainPrompt.AppendBreak();
                saySpeechGreeting.MainPrompt.AppendAudio(basePath + "\\Prompts\\recmess.wav");
            }

        }

        private void askMenuOptions_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            this.askMenuOptions.MainPrompt.ClearContent();
            this.askMenuOptions.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\tosend.wav"), "To send your message, press pound, Or");

            this.askMenuOptions.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\toreplay.wav"), "To replay press 1");
            this.askMenuOptions.MainPrompt.AppendBreak();
            this.askMenuOptions.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\torecordover.wav"), "To record over press 2");
            this.askMenuOptions.MainPrompt.AppendBreak();

            this.askMenuOptions.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\tomarkurgent.wav"), "To mark urgent press 3");

            //this.askMenuOptions.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\tocancel.wav"),;
            //this.askMenuOptions.MainPrompt.AppendAudio(basePath + "\\Prompts\\toenterphone.wav");
            //this.askMenuOptions.MainPrompt.AppendAudio(basePath + "\\Prompts\\totransoper.wav");


            this.askMenuOptions.DtmfGrammars.Clear();
            this.askMenuOptions.DtmfGrammars.Add(new Grammar(
                    basePath + @"\\Grammars\PinDtmf.grxml", "digitsplus"));

        }

        private string getUserByCellPhone()
        {
            string upn;
            string hostingPath;

            hostingPath = ConfigurationManager.AppSettings["hostingPath"];
            //writeLog("Hosting path: " + hostingPath);
            //writeLog(upn);

            DirectoryEntry dEntry = new DirectoryEntry();
            //dEntry.Path = string.Format("LDAP://{0},{1}", hostingPath.Trim(), tenant);
            dEntry.Path = string.Format("LDAP://{0}", hostingPath.Trim());
            dEntry.AuthenticationType = AuthenticationTypes.None;

            DirectorySearcher dSearcher = new DirectorySearcher(dEntry);
            dSearcher.SearchScope = SearchScope.Subtree;
            dSearcher.ReferralChasing = ReferralChasingOption.All;

            dSearcher.Filter = "(mobilephone=" + transferredBy + ")";
            dSearcher.PropertiesToLoad.Add("userPrincipalName");
            upn = string.Empty;
            try
            {
                SearchResult result = dSearcher.FindOne();
                DirectoryEntry directoryEntry = result.GetDirectoryEntry();
                upn = string.Format("Sip:{01}", directoryEntry.Properties["userPrincipalName"][0].ToString());
            }
            catch (NullReferenceException ex)
            {
                //writeLog("Unable to locate user email address: " + ex.ToString());
            }
            return upn;
        }

        private string getCustomGreeting()
        {
            int startPos;
            string customGreetingPath = string.Empty;
            startPos = (transferredBy.IndexOf(":"));

            customGreetingPath = string.Format("C:\\Greetings\\{0}\\CustomGreeting.wav", transferredBy.Substring(startPos + 1));
            writeLog(string.Format("** CustomGreeting:{0}", customGreetingPath));
            // Now letsmake sure the file exists
            if (File.Exists(customGreetingPath))
                return customGreetingPath;
            else
                return string.Empty;

            //customGreetingPath = "klaw@uberuc.com";
        }

        private void sayBeep2_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            this.sayBeep2.MainPrompt.ClearContent();
            sayBeep2.MainPrompt.AppendAudio(basePath + "\\Prompts\\beep3.wav");
            sayBeep2.MainPrompt.AppendAudio(basePath + "\\Prompts\\beep3.wav");
        }

        private void sayRecordedMessage_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            this.sayRecordedMessage.MainPrompt.ClearContent();
            this.sayRecordedMessage.MainPrompt.AppendAudio(this.recordActivity2.FileName.ToString());
        }

        private void sayUrgentMsg_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            this.sayUrgentMsg.MainPrompt.ClearContent();
            this.sayUrgentMsg.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\markUrgent.wav"), "Message marked urgent");
        }

        private void sayCancelMessage_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            this.sayCancelMessage.MainPrompt.ClearContent();
            this.sayCancelMessage.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\emailCancel.wav"), "Canceling Voice Mail");
        }

        private void sayMessageSent_TurnStarting(object sender, Microsoft.Rtc.Workflow.Activities.SpeechTurnStartingEventArgs e)
        {
            this.sayMessageSent.MainPrompt.ClearContent();
            this.sayMessageSent.MainPrompt.AppendAudio(new Uri(basePath + "\\Prompts\\messageSent.wav"), "Message sent");

        }

        #endregion

        #region SR_Logic
        private void codeSetFileNameForRecording_ExecuteCode(object sender, EventArgs e)
        {
            // Obtain a unique file name for the recording
            this.recordActivity2.FileName = GetUniqueFileName();
            //writeLog("codeSetFileNameForRecording_ExecuteCode: " + this.recordActivity2.FileName.ToString());
        }


        private string GetUniqueFileName()
        {
            Guid guid = Guid.NewGuid();
            return string.Format("C:\\CallTower\\Recordings\\{0}.wma", guid.ToString());
        }

        private void codeCreateEmail_ExecuteCode(object sender, EventArgs e)
        {
            // Specify the file to be attached and sent.
            string recordingFileName = this.recordActivity2.FileName;
            //            string imageFileName = "\\Images\\calltower.jpg";
            string imageFileName = "\\Images\\nextuc-logo.jpg";
            string emailPath = string.Empty;
            string callFrom = string.Empty;
            string callTo = string.Empty;
            int startPos;
            int endPos;
            bool potsCall = false;
            string mailBody = string.Empty;
            string lineURI = string.Empty;
            string subjectLine = String.Empty;

            writeLog("codeCreateEmail_ExecuteCode");

            // If we don't have a recording then we sgould have the default file name
            // This mans that the user hung up before the recording started.

            if (recordingFileName.Contains("ing\\test"))
            {
                missedCall = true;
                writeLog("missedCall = true");
                //return;
            }

            if (remoteParticipant.Contains("sip:+") || remoteParticipant.Contains("tel:+"))
            { // Telephone call

                //writeLog(string.Format("**{0} ==> {1} ==> {2}", remoteParticipant, toHeader, transferredBy));
                potsCall = true;
                // Get the "To:" information for the email. We need to send
                // it to the original recipient of te call
                startPos = transferredBy.IndexOf(":");
                callTo = getEmailAddress(transferredBy.Substring(startPos + 1));
                if (!(callTo.Contains("@")))
                {
                    callTo = ConfigurationManager.AppSettings["alertEmail"];
                }

                // Get the "From:" info for the call. This will be the 
                // originator of the call
                startPos = (remoteParticipant.IndexOf(":") + 3); // nee dto skip the ":+1"
                if ((remoteParticipant.Contains("@")))
                {
                    endPos = (remoteParticipant.IndexOf("@"));
                    callFrom = remoteParticipant.Substring(startPos, (endPos - startPos)) + ConfigurationManager.AppSettings["fromEmailDomain"];
                }
                else
                {
                    //  callFrom = remoteParticipant.Substring(startPos, (endPos - startPos)) + "@voicemail.uberuc.com";
                    callFrom = remoteParticipant.Substring(startPos) + ConfigurationManager.AppSettings["fromEmailDomain"];
                }
            }
            else
            { // Lync to Lync call

                //writeLog(string.Format("******** To Header:{0}", toHeader));
                //writeLog(string.Format("******** TransferredBy:{0}", transferredBy));
                //writeLog(string.Format("******** RemoteParticipant:{0}", remoteParticipant));
                //writeLog(string.Format("**{0} ==> {1}", remoteParticipant, toHeader));


                // Get the "To:" information for the email. We need to send
                // it to the original recipient of the call
                startPos = transferredBy.IndexOf(":");
                callTo = getEmailAddress(transferredBy.Substring(startPos + 1));
                if (!(callTo.Contains("@")))
                {
                    callTo = ConfigurationManager.AppSettings["alertEmail"];
                }


                // Get the "From:" info for the call. This will be the 
                // originator of the call
                startPos = remoteParticipant.IndexOf(":");
                // Note: if getEmailAddress can't find the user in our AD then it returns an empty string
                callFrom = getEmailAddress(remoteParticipant.Substring(startPos + 1));
                // If not found then just use the SIP address
                if (callFrom.Length == 0)
                {
                    // Check for ";user=phone"
                    if (remoteParticipant.Contains(";"))
                    {
                        endPos = remoteParticipant.IndexOf(";");
                        callFrom = remoteParticipant.Substring(startPos+1, (endPos - (startPos +1)));
                    }
                    else
                        callFrom = remoteParticipant.Substring(startPos + 1);
                }

                // Get the caller's telephone number from the AD
                // If not found it is set to string.Empty
                lineURI = getCallBackNumber(remoteParticipant.Substring(startPos + 1));
            }

            writeLog(string.Format("******** CallTo:{0}", callTo));
            writeLog(string.Format("******** CallFrom:{0}", callFrom));
            //writeLog(string.Format("******** lineURI:{0}", lineURI));


            // Create the message body
            if (potsCall)
                mailBody = buildPotsEmail(callFrom);
            else
                mailBody = buildLyncEmail(callFrom, lineURI);

            //// Create a message and set up the recipients.
            if (missedCall)
                subjectLine = "Missed Call";
            else
                subjectLine = "Voice Message";

            MailMessage message = new MailMessage(
               callFrom,
               callTo,
               subjectLine,
               mailBody);

            // Set the content encoding and thebody type
            message.BodyEncoding = System.Text.Encoding.ASCII;
            message.IsBodyHtml = true;

            // Create the Message-ID header which needs a 
            // specific format to work with our voice mail
            Guid guid = Guid.NewGuid();
            message.Headers.Add("Message-ID", string.Format("CallTowerVM.{0}", guid.ToString()));
            message.Headers.Add("X-Voicemail-Compression-Type", "audio/mp3");
            message.Headers.Add("X-Voicemail-Transcribe", "true");
            // Set the Message priority
            if (emailIsUrgent)
                message.Priority = MailPriority.High;

            // If this is a missed call then nothing 
            // to attach so skip this
            if (!missedCall)
            {
                // Create  the file attachment 
                Attachment audio = new Attachment(recordingFileName);
                //audio.ContentType.MediaType = "audio/wma";
                audio.TransferEncoding = TransferEncoding.Base64;

                message.Attachments.Add(audio);
            }

            Attachment image = new Attachment(basePath + imageFileName, "image/jpg");
            image.ContentId = "logo@nextuc.com";
            message.Attachments.Add(image);

            // Now send the email to a directory on the server
            SmtpClient client = new SmtpClient("localhost");
            client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
            if (missedCall)
                emailPath = ConfigurationManager.AppSettings["smtpPath"];
            else
                emailPath = ConfigurationManager.AppSettings["emailPath"];
            writeLog("Wrote Email to: " + emailPath);

            //client.PickupDirectoryLocation = @"C:\CallTower\EmailDrop";
            client.PickupDirectoryLocation = emailPath;
            client.Send(message);
            sentEmail = true;

        }

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Note: this is used for boththe calling and called party email addresses
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        string getEmailAddress(string upn)
        {
            string mail;
            string hostingPath;

            hostingPath = ConfigurationManager.AppSettings["hostingPath"];
            //writeLog("Hosting path: " + hostingPath);
            //writeLog(upn);

            DirectoryEntry dEntry = new DirectoryEntry();
            //dEntry.Path = string.Format("LDAP://{0},{1}", hostingPath.Trim(), tenant);
            dEntry.Path = string.Format("LDAP://{0}", hostingPath.Trim());
            dEntry.AuthenticationType = AuthenticationTypes.None;

            DirectorySearcher dSearcher = new DirectorySearcher(dEntry);
            dSearcher.SearchScope = SearchScope.Subtree;
            dSearcher.ReferralChasing = ReferralChasingOption.All;
            dSearcher.Filter = "(userPrincipalName=" + upn + ")";
            dSearcher.PropertiesToLoad.Add("mail");
            mail = string.Empty;
            try
            {
                SearchResult result = dSearcher.FindOne();
                DirectoryEntry directoryEntry = result.GetDirectoryEntry();
                mail = directoryEntry.Properties["mail"][0].ToString();
            }
            catch (NullReferenceException ex)
            {
                //writeLog("Unable to locate user email address: " + ex.ToString());
            }
            return mail;
        }





        string getCallBackNumber(string upn)
        {
            string callBackNumber;
            string hostingPath;

            hostingPath = ConfigurationManager.AppSettings["hostingPath"];
            //writeLog("Hosting path: " + hostingPath);

            DirectoryEntry dEntry = new DirectoryEntry();
            //dEntry.Path = string.Format("LDAP://{0},{1}", hostingPath.Trim(), tenant);
            dEntry.Path = string.Format("LDAP://{0}", hostingPath.Trim());
            dEntry.AuthenticationType = AuthenticationTypes.None;

            DirectorySearcher dSearcher = new DirectorySearcher(dEntry);
            dSearcher.SearchScope = SearchScope.Subtree;
            dSearcher.ReferralChasing = ReferralChasingOption.All;
            dSearcher.Filter = "(userPrincipalName=" + upn + ")";
            dSearcher.PropertiesToLoad.Add("msRTCSIP-Line");
            callBackNumber = string.Empty;
            try
            {
                SearchResult result = dSearcher.FindOne();
                DirectoryEntry directoryEntry = result.GetDirectoryEntry();
                callBackNumber = directoryEntry.Properties["msRTCSIP-Line"][0].ToString();
                //writeLog("CallBackNumber: " + callBackNumber);
            }
//            catch (NullReferenceException ex)
            catch (Exception ex)
            {
                writeLog("Unable to locateb user DID number: ");
            }
            return callBackNumber;
        }

        string buildPotsEmail(string callFrom)
        {
            StringBuilder mailBody = new StringBuilder();
            int endPos;
            endPos = callFrom.IndexOf("@");
            if (missedCall)
            {
                writeLog("buildPotsEmail: MissedCall");
                mailBody.AppendFormat("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
                mailBody.AppendFormat("<html><head><meta name=\"type\" content=\"vminfo\"/><meta name=\"callback\" content=\"4054759264\"/>");
                mailBody.AppendFormat("<title>VoiceMail</title></head>");
                mailBody.AppendFormat("<body>");
                mailBody.AppendFormat(string.Format("<br><b>You missed a call from {0}</b><br><br>", callFrom.Substring(0, endPos)));
                mailBody.AppendFormat(string.Format("Callback Number: tel:+1{0}<br>", callFrom.Substring(0, endPos)));
                //mailBody.AppendFormat(string.Format("Callback address: {0}<br>", "Sip:marshall@gotuc.net"));
                mailBody.AppendFormat("<br><img border=0 width=200 height=80 id=\"logo\" src=\"cid:logo@nextuc.com\" alt=\"NextUC\"><br></body></html>");
            }
            else
            {
                writeLog("buildPotsEmail");
                // Create the message body
                mailBody.AppendFormat("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
                mailBody.AppendFormat("<html><head><meta name=\"type\" content=\"vminfo\"/><meta name=\"callback\" content=\"4054759264\"/>");
                mailBody.AppendFormat("<title>VoiceMail</title></head>");
                mailBody.AppendFormat("<body>");
                mailBody.AppendFormat("This is a NextUC voice message.  To listen right click on the attachment and either open or save.<br><br>");
                mailBody.AppendFormat(string.Format("Callback Number: tel:+1{0}<br>", callFrom.Substring(0, endPos)));
                //mailBody.AppendFormat(string.Format("Callback address: {0}<br>", "Sip:marshall@gotuc.net"));
                mailBody.AppendFormat("<br><img border=0 width=200 height=80 id=\"logo\" src=\"cid:logo@nextuc.com\" alt=\"NextUC\"><br></body></html>");
            }
            return mailBody.ToString();
        }


        string buildLyncEmail(string callFrom, string lineURI)
        {
            StringBuilder mailBody = new StringBuilder();

            if (missedCall)
            {
                writeLog("buildLyncEmail: missedCall");
                // Create the message body
                mailBody.AppendFormat("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
                mailBody.AppendFormat("<html><head><meta name=\"type\" content=\"vminfo\"/><meta name=\"callback\" content=\"4054759264\"/>");
                mailBody.AppendFormat("<title>VoiceMail</title></head>");
                mailBody.AppendFormat(string.Format("<body>"));
                mailBody.AppendFormat(string.Format("<br><b>You missed a call from {0}</b><br><br>", callFrom));
                if (lineURI.Length > 0)
                    mailBody.AppendFormat(string.Format("Work&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   <a href={0}>{1}</a><br>", lineURI, lineURI));
                mailBody.AppendFormat(string.Format("E-Mail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   <a href=mailto:{0}>{1}</a><br>", callFrom, callFrom));
                mailBody.AppendFormat(string.Format("IM Address:   <a href=sip:{0}>{1}</a><br>", callFrom, callFrom));
                mailBody.AppendFormat("<br><img border=0 width=200 height=80 id=\"logo\" src=\"cid:logo@nextuc.com\" alt=\"NextUC\"><br></body></html>");
            }
            else
            {
                writeLog("buildLyncEmail");
                // Create the message body
                mailBody.AppendFormat("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
                mailBody.AppendFormat("<html><head><meta name=\"type\" content=\"vminfo\"/><meta name=\"callback\" content=\"4054759264\"/>");
                mailBody.AppendFormat("<title>VoiceMail</title></head>");
                mailBody.AppendFormat("<body>");
                mailBody.AppendFormat(  string.Format("You have received a voice mail from {0}<br><br>", callFrom));
                if (lineURI.Length > 0)
                    mailBody.AppendFormat(string.Format("Work&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   <a href={0}>{1}</a><br>", lineURI, lineURI));
                mailBody.AppendFormat(string.Format("E-Mail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:   <a href=mailto:{0}>{1}</a><br>", callFrom, callFrom));
                mailBody.AppendFormat(string.Format("IM Address:   <a href=sip:{0}>{1}</a><br>", callFrom, callFrom));
                mailBody.AppendFormat("<br><img border=0 width=200 height=80 id=\"logo\" src=\"cid:logo@nextuc.com\" alt=\"NextUC\"><br></body></html>");
            }
            return mailBody.ToString();
        }




        private void codeCancelEmail_ExecuteCode(object sender, EventArgs e)
        {
            // Since we hang up write after the cancel prompt
            // I set sentEmail to true so that the disconect
            // handler does try to send the email
            sentEmail = true;
        }

        private void codeMarkEmailUrgent_ExecuteCode(object sender, EventArgs e)
        {
            this.emailIsUrgent = true;
        }

        #endregion


        #region SR_decisionlogic
        private void ifCasePound(object sender, ConditionalEventArgs e)
        {
            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "pound")
                e.Result = true;
            else
                e.Result = false;

        }

        private void ifCase0(object sender, ConditionalEventArgs e)
        {
            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "0")
                e.Result = true;
            else
                e.Result = false;

        }

        private void ifCase1(object sender, ConditionalEventArgs e)
        {
            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "1")
                e.Result = true;
            else
                e.Result = false;

        }

        private void ifCase2(object sender, ConditionalEventArgs e)
        {
            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "2")
                e.Result = true;
            else
                e.Result = false;

        }

        private void ifCase3(object sender, ConditionalEventArgs e)
        {

            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "3")
                e.Result = true;
            else
                e.Result = false;
        }

        private void ifCase4(object sender, ConditionalEventArgs e)
        {

            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "4")
                e.Result = true;
            else
                e.Result = false;
        }

        private void ifCase7(object sender, ConditionalEventArgs e)
        {

            if (askMenuOptions.RecognitionResult.Semantics.Value.ToString() == "7")
                e.Result = true;
            else
                e.Result = false;
        }
        #endregion

        private void codeCheckForCellVoiceMail_ExecuteCode(object sender, EventArgs e)
        {
            string tempStr = string.Empty;

            // To keep the logic cleaner in case of a cell phone voice mail then we will just
            // fake the transferred by to look like a call to teh user's Lync account. That way all of the 
            // custom greeting and email type logic will work without changes.
            if (toHeader.Contains(ConfigurationManager.AppSettings["voiceMailNumber"].ToString()) ||
                toHeader.Contains(ConfigurationManager.AppSettings["voiceMailUser"].ToString()))
            {
                tempStr = getUserByCellPhone();
                if (tempStr != string.Empty)
                    transferredBy = tempStr;
            }
        }



    }
        #endregion
}

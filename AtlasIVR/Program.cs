using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Reflection;
using log4net; 

namespace CommunicationServiceWithConsole
{
    static class Program
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceBase[] servicesToRun = new ServiceBase[] { new WindowsService() }; // This holds the array of services to start
            log4net.Config.XmlConfigurator.Configure();
            _logger.Info("Windows Service Console");
            _logger.Info("--------------------------------------");
            if (Environment.UserInteractive)
            {
                log4net.Config.XmlConfigurator.Configure();
                _logger.Info("Windows Service Console");
                _logger.Info("--------------------------------------");
                
                Type type = typeof(ServiceBase);
                BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
                MethodInfo startmethod = type.GetMethod("OnStart", flags);
                MethodInfo stopmethod = type.GetMethod("OnStop", flags);

                foreach (ServiceBase service in servicesToRun)
                {
                    _logger.InfoFormat("Starting {0}...", service.ServiceName);
                    startmethod.Invoke(service, new object[] { args });
                }

                Console.WriteLine("Press any key to exit");
                Console.Read();

                foreach (ServiceBase service in servicesToRun)
                {
                    _logger.InfoFormat("Stopping {0}...", service.ServiceName);
                    stopmethod.Invoke(service, new object[] { });
                    Console.WriteLine();
                }
            }
            else
            {
                ServiceBase.Run(servicesToRun);
            }
        }
       }
}
